% Game settings

time_to_think(1).

vision_length(3).

mountain_resting_time(1).

unconscious_time(10).

climbing_time(1).

fight_skill_reward(1).


registration_handler_freq(2).

dice_sides(50).

% Esto implica que si un agente le lleva 50 de fight_skill a otro
% entonces siempre le ganar�.

hostel_recovery_rate(20).

%max_stamina(50).

%max_stamina(MS):- n_of_arrows(R), n_of_columns(C),
%                  MS is round(R*C).

:- dynamic initial_max_stamina/2.

:- n_of_arrows(R),
   n_of_columns(C),
   MS is round(R*C/3),%%Original.
   %MS is round(R*C/6),
   assert(initial_max_stamina(agent, MS)),
   write('asserto initial_max_stamina').

initial_max_stamina(dragon, 1000).

% Deprecated predicate. Only for compatibility reasons (used from JAVA)
max_stamina(AgType, MS):- initial_max_stamina(AgType, MS).

initial_fight_skill(100).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fight_skill_function(+FightsWon, -FightSkill)
%
fight_skill_function(AttacksWon, FightSkill):-
                                initial_fight_skill(InitialFightSkill),
                                FightSkill is floor(InitialFightSkill + (AttacksWon*1.5)^(3/4)).

max_stamina_function(AgType, TrainingActions, MaxStamina):-
                                initial_max_stamina(AgType, InitialMaxStamina),
                                MaxStamina is floor(InitialMaxStamina + ((1 + TrainingActions)/4)^(3/4)).

% Se suma 1 a TrainingActions para evitar excepci�n al dividir si viene
% en 0.

%forbidden_entry_time(FET):- max_stamina(MS),
%                            FET is round(MS/2).

:- initial_max_stamina(agent, MS),
   FET is round(MS/2),
   assert(forbidden_entry_time(FET)).

%wake_up_stamina(20).
%wake_up_stamina(WS):- max_stamina(MS),
%                      WS is round(MS * 0.2).

%:- initial_max_stamina(agent ,MS),
%   WS is round(MS * 0.2),
%   assert(wake_up_stamina(WS)).


wake_up_stamina(AgType, WS):-
        initial_max_stamina(AgType, MS),
        WS is round(MS * 0.2).

% Costo total en stamina de la accion

stamina_cost(attack, 1).
stamina_cost(turn, 1).
stamina_cost(move_fwd_plain, 1).
stamina_cost(move_fwd_mountain, 2).
stamina_cost(pickup, 0).
stamina_cost(drop, 0).
stamina_cost(cast_spell, 0).
stamina_cost(take_from, 0).

% Tiempo adicional que consume la accion (1 de Base)
% Es decir, cantidad de turnos adicionales durante los cuales
% se estara ejecutando la accion

time_cost(attack, 0).
time_cost(turn, 0).
time_cost(move_fwd_plain, 0).
time_cost(move_fwd_mountain, 1).
time_cost(pickup, 0).
time_cost(drop, 0).
time_cost(cast_spell, 0).
time_cost(take_from, 1).



%ags_starting_pos([8,7]).

ags_starting_positions([[[8,7], n], [[8,9], e], [[10,9], s], [[10,7], w], [[11,8], s], [[7,11], n]]).

desired_amount_of_potions(25).




















