%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%
%% Configuración de la Comarca
/*

       1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
     1 #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
     2 #  -  -  -  -  -  -  #  h  -  -  -  -  -  -  -  -  -  t  #
     3 #  -  #  -  -  -  -  #  -  -  -  -  -  -  -  -  -  #  #  #
     4 #  -  #  #  #  #  -  -  #  ^  -  #  #  #  #  -  -  -  -  #
     5 #  -  #  -  -  #  -  -  -  -  -  -  -  -  #  t  #  #  -  #
     6 #  -  -  -  -  #  -  -  ^  -  -  -  -  -  -  #  #  -  -  #
     7 #  -  -  #  #  #  -  ^  ^  -  -  -  -  -  -  -  -  -  -  #
     8 #  -  -  #  -  -  -  ^  -  -  -  -  -  W  W  W  W  W  W  W
     9 #  -  -  -  -  -  ^  -  -  -  -  -  -  W  t  -  ^  #  t  #
    10 #  -  -  W  -  -  -  -  -  -  -  -  h  W  -  #  ^  #  -  #
    11 W  -  W  W  W  W  -  -  -  -  -  -  W  W  -  #  ^  -  -  #
    12 #  -  -  #  h  W  W  -  #  #  -  -  W  -  -  ^  #  -  #  #
    13 #  -  -  -  -  -  W  -  #  -  -  -  -  -  #  -  -  -  -  #
    14 #  -  -  -  -  ^  ^  ^  -  -  -  -  W  -  -  -  #  -  t  #
    15 #  #  #  #  #  #  W  #  #  #  #  #  W  #  #  #  #  #  #  #
*/


n_of_arrows(21).

n_of_columns(30).

% cell_land(Pos, Land)

% Land:
%
% plain
% water
% mountain
% forest


cell_land([1,1], forest).
cell_land([1,2], forest).
cell_land([1,3], water).
cell_land([1,4], forest).
cell_land([1,5], forest).
cell_land([1,6], forest).
cell_land([1,7], forest).
cell_land([1,8], forest).
cell_land([1,9], forest).
cell_land([1,10], forest).
cell_land([1,11], forest).
cell_land([1,12], forest).
cell_land([1,13], forest).
cell_land([1,14], forest).
cell_land([1,15], forest).
cell_land([1,16], forest).
cell_land([1,17], forest).
cell_land([1,18], forest).
cell_land([1,19], forest).
cell_land([1,20], forest).
cell_land([1,21], forest).
cell_land([1,22], forest).
cell_land([1,23], forest).
cell_land([1,24], forest).
cell_land([1,25], forest).
cell_land([1,26], forest).
cell_land([1,27], forest).
cell_land([1,28], forest).
cell_land([1,29], forest).
cell_land([1,30], forest).

cell_land([2,1], forest).
cell_land([2,2], plain).
cell_land([2,3], plain).
cell_land([2,4], plain).
cell_land([2,5], mountain).
cell_land([2,6], plain).
cell_land([2,7], plain).
cell_land([2,8], forest).
cell_land([2,9], plain).
cell_land([2,10], plain).
cell_land([2,11], plain).
cell_land([2,12], plain).
cell_land([2,13], plain).
cell_land([2,14], plain).
cell_land([2,15], plain).
cell_land([2,16], plain).
cell_land([2,17], plain).
cell_land([2,18], plain).
cell_land([2,19], plain).
cell_land([2,20], forest).
cell_land([2,21], plain).
cell_land([2,22], plain).
cell_land([2,23], plain).
cell_land([2,24], plain).
cell_land([2,25], plain).
cell_land([2,26], plain).
cell_land([2,27], plain).
cell_land([2,28], plain).
cell_land([2,29], plain).
cell_land([2,30], forest).



cell_land([3,1], forest).
cell_land([3,2], plain).
cell_land([3,3], water).
cell_land([3,4], plain).
cell_land([3,5], plain).
cell_land([3,6], plain).
cell_land([3,7], plain).
cell_land([3,8], forest).
cell_land([3,9], plain).
cell_land([3,10], plain).
cell_land([3,11], plain).
cell_land([3,12], plain).
cell_land([3,13], plain).
cell_land([3,14], plain).
cell_land([3,15], plain).
cell_land([3,16], plain).
cell_land([3,17], plain).
cell_land([3,18], forest).
cell_land([3,19], forest).
cell_land([3,20], forest).
cell_land([3,21], plain).
cell_land([3,22], plain).
cell_land([3,23], plain).
cell_land([3,24], plain).
cell_land([3,25], plain).
cell_land([3,26], plain).
cell_land([3,27], plain).
cell_land([3,28], plain).
cell_land([3,29], plain).
cell_land([3,30], forest).


cell_land([4,1], forest).
cell_land([4,2], plain).
cell_land([4,3], water).
cell_land([4,4], water).
cell_land([4,5], water).
cell_land([4,6], water).
cell_land([4,7], plain).
cell_land([4,8], plain).
cell_land([4,9], forest).
cell_land([4,10], mountain).
cell_land([4,11], plain).
cell_land([4,12], forest).
cell_land([4,13], forest).
cell_land([4,14], forest).
cell_land([4,15], forest).
cell_land([4,16], plain).
cell_land([4,17], plain).
cell_land([4,18], plain).
cell_land([4,19], plain).
cell_land([4,20], forest).
cell_land([4,21], plain).
cell_land([4,22], plain).
cell_land([4,23], plain).
cell_land([4,24], plain).
cell_land([4,25], plain).
cell_land([4,26], plain).
cell_land([4,27], plain).
cell_land([4,28], plain).
cell_land([4,29], plain).
cell_land([4,30], forest).


cell_land([5,1], forest).
cell_land([5,2], plain).
cell_land([5,3], forest).
cell_land([5,4], plain).
cell_land([5,5], plain).
cell_land([5,6], water).
cell_land([5,7], plain).
cell_land([5,8], plain).
cell_land([5,9], plain).
cell_land([5,10], plain).
cell_land([5,11], plain).
cell_land([5,12], plain).
cell_land([5,13], plain).
cell_land([5,14], plain).
cell_land([5,15], forest).
cell_land([5,16], plain).
cell_land([5,17], forest).
cell_land([5,18], forest).
cell_land([5,19], plain).
cell_land([5,20], forest).
cell_land([5,21], plain).
cell_land([5,22], plain).
cell_land([5,23], plain).
cell_land([5,24], plain).
cell_land([5,25], plain).
cell_land([5,26], plain).
cell_land([5,27], plain).
cell_land([5,28], plain).
cell_land([5,29], plain).
cell_land([5,30], forest).



cell_land([6,1], forest).
cell_land([6,2], plain).
cell_land([6,3], plain).
cell_land([6,4], plain).
cell_land([6,5], plain).
cell_land([6,6], water).
cell_land([6,7], plain).
cell_land([6,8], plain).
cell_land([6,9], mountain).
cell_land([6,10], plain).
cell_land([6,11], plain).
cell_land([6,12], plain).
cell_land([6,13], plain).
cell_land([6,14], plain).
cell_land([6,15], plain).
cell_land([6,16], forest).
cell_land([6,17], forest).
cell_land([6,18], plain).
cell_land([6,19], plain).
cell_land([6,20], plain).
cell_land([6,21], plain).
cell_land([6,22], plain).
cell_land([6,23], plain).
cell_land([6,24], plain).
cell_land([6,25], plain).
cell_land([6,26], plain).
cell_land([6,27], plain).
cell_land([6,28], plain).
cell_land([6,29], plain).
cell_land([6,30], forest).


cell_land([7,1], forest).
cell_land([7,2], plain).
cell_land([7,3], plain).
cell_land([7,4], water).
cell_land([7,5], water).
cell_land([7,6], water).
cell_land([7,7], plain).
cell_land([7,8], mountain).
cell_land([7,9], mountain).
cell_land([7,10], plain).
cell_land([7,11], plain).
cell_land([7,12], plain).
cell_land([7,13], plain).
cell_land([7,14], plain).
cell_land([7,15], plain).
cell_land([7,16], plain).
cell_land([7,17], plain).
cell_land([7,18], plain).
cell_land([7,19], plain).
cell_land([7,20], plain).
cell_land([7,21], plain).
cell_land([7,22], plain).
cell_land([7,23], plain).
cell_land([7,24], plain).
cell_land([7,25], plain).
cell_land([7,26], plain).
cell_land([7,27], plain).
cell_land([7,28], plain).
cell_land([7,29], plain).
cell_land([7,30], forest).


cell_land([8,1], forest).
cell_land([8,2], plain).
cell_land([8,3], plain).
cell_land([8,4], water).
cell_land([8,5], plain).
cell_land([8,6], plain).
cell_land([8,7], plain).
cell_land([8,8], mountain).
cell_land([8,9], plain).
cell_land([8,10], plain).
cell_land([8,11], plain).
cell_land([8,12], plain).
cell_land([8,13], plain).
cell_land([8,14], water).
cell_land([8,15], water).
cell_land([8,16], water).
cell_land([8,17], water).
cell_land([8,18], water).
cell_land([8,19], water).
cell_land([8,20], water).
cell_land([8,21], water).
cell_land([8,22], water).
cell_land([8,23], water).
cell_land([8,24], water).
cell_land([8,25], plain).
cell_land([8,26], plain).
cell_land([8,27], plain).
cell_land([8,28], plain).
cell_land([8,29], plain).
cell_land([8,30], forest).


cell_land([9,1], forest).
cell_land([9,2], plain).
cell_land([9,3], plain).
cell_land([9,4], plain).
cell_land([9,5], plain).
cell_land([9,6], plain).
cell_land([9,7], mountain).
cell_land([9,8], plain).
cell_land([9,9], plain).
cell_land([9,10], plain).
cell_land([9,11], plain).
cell_land([9,12], plain).
cell_land([9,13], plain).
cell_land([9,14], water).
cell_land([9,15], plain).
cell_land([9,16], plain).
cell_land([9,17], mountain).
cell_land([9,18], forest).
cell_land([9,19], plain).
cell_land([9,20], forest).
cell_land([9,21], plain).
cell_land([9,22], plain).
cell_land([9,23], plain).
cell_land([9,24], water).
cell_land([9,25], water).
cell_land([9,26], plain).
cell_land([9,27], plain).
cell_land([9,28], plain).
cell_land([9,29], plain).
cell_land([9,30], forest).



cell_land([10,1], forest).
cell_land([10,2], plain).
cell_land([10,3], plain).
cell_land([10,4], water).
cell_land([10,5], plain).
cell_land([10,6], plain).
cell_land([10,7], plain).
cell_land([10,8], plain).
cell_land([10,9], plain).
cell_land([10,10], plain).
cell_land([10,11], plain).
cell_land([10,12], plain).
cell_land([10,13], plain).
cell_land([10,14], water).
cell_land([10,15], plain).
cell_land([10,16], forest).
cell_land([10,17], mountain).
cell_land([10,18], forest).
cell_land([10,19], plain).
cell_land([10,20], forest).
cell_land([10,21], plain).
cell_land([10,22], plain).
cell_land([10,23], plain).
cell_land([10,24], plain).
cell_land([10,25], water).
cell_land([10,26], water).
cell_land([10,27], water).
cell_land([10,28], water).
cell_land([10,29], plain).
cell_land([10,30], water).


cell_land([11,1], water).
cell_land([11,2], plain).
cell_land([11,3], water).
cell_land([11,4], water).
cell_land([11,5], water).
cell_land([11,6], water).
cell_land([11,7], plain).
cell_land([11,8], plain).
cell_land([11,9], plain).
cell_land([11,10], plain).
cell_land([11,11], plain).
cell_land([11,12], plain).
cell_land([11,13], water).
cell_land([11,14], water).
cell_land([11,15], plain).
cell_land([11,16], forest).
cell_land([11,17], mountain).
cell_land([11,18], plain).
cell_land([11,19], plain).
cell_land([11,20], forest).
cell_land([11,21], plain).
cell_land([11,22], plain).
cell_land([11,23], plain).
cell_land([11,24], plain).
cell_land([11,25], plain).
cell_land([11,26], plain).
cell_land([11,27], plain).
cell_land([11,28], plain).
cell_land([11,29], plain).
cell_land([11,30], forest).



cell_land([12,1], forest).
cell_land([12,2], plain).
cell_land([12,3], plain).
cell_land([12,4], forest).
cell_land([12,5], plain).
cell_land([12,6], water).
cell_land([12,7], water).
cell_land([12,8], plain).
cell_land([12,9], forest).
cell_land([12,10], forest).
cell_land([12,11], plain).
cell_land([12,12], plain).
cell_land([12,13], water).
cell_land([12,14], plain).
cell_land([12,15], plain).
cell_land([12,16], mountain).
cell_land([12,17], forest).
cell_land([12,18], plain).
cell_land([12,19], forest).
cell_land([12,20], forest).
cell_land([12,21], plain).
cell_land([12,22], plain).
cell_land([12,23], plain).
cell_land([12,24], plain).
cell_land([12,25], plain).
cell_land([12,26], plain).
cell_land([12,27], plain).
cell_land([12,28], plain).
cell_land([12,29], plain).
cell_land([12,30], forest).


cell_land([13,1], forest).
cell_land([13,2], plain).
cell_land([13,3], plain).
cell_land([13,4], plain).
cell_land([13,5], plain).
cell_land([13,6], plain).
cell_land([13,7], water).
cell_land([13,8], plain).
cell_land([13,9], forest).
cell_land([13,10], plain).
cell_land([13,11], plain).
cell_land([13,12], plain).
cell_land([13,13], plain).
cell_land([13,14], plain).
cell_land([13,15], forest).
cell_land([13,16], plain).
cell_land([13,17], plain).
cell_land([13,18], plain).
cell_land([13,19], plain).
cell_land([13,20], forest).
cell_land([13,21], plain).
cell_land([13,22], plain).
cell_land([13,23], plain).
cell_land([13,24], plain).
cell_land([13,25], plain).
cell_land([13,26], plain).
cell_land([13,27], plain).
cell_land([13,28], plain).
cell_land([13,29], plain).
cell_land([13,30], forest).


cell_land([14,1], forest).
cell_land([14,2], plain).
cell_land([14,3], plain).
cell_land([14,4], plain).
cell_land([14,5], plain).
cell_land([14,6], mountain).
cell_land([14,7], mountain).
cell_land([14,8], mountain).
cell_land([14,9], plain).
cell_land([14,10], plain).
cell_land([14,11], plain).
cell_land([14,12], plain).
cell_land([14,13], water).
cell_land([14,14], plain).
cell_land([14,15], plain).
cell_land([14,16], plain).
cell_land([14,17], forest).
cell_land([14,18], plain).
cell_land([14,19], plain).
cell_land([14,20], forest).
cell_land([14,21], plain).
cell_land([14,22], plain).
cell_land([14,23], plain).
cell_land([14,24], plain).
cell_land([14,25], plain).
cell_land([14,26], plain).
cell_land([14,27], plain).
cell_land([14,28], plain).
cell_land([14,29], plain).
cell_land([14,30], forest).


cell_land([15,1], forest).
cell_land([15,2], plain).
cell_land([15,3], plain).
cell_land([15,4], forest).
cell_land([15,5], forest).
cell_land([15,6], mountain).
cell_land([15,7], water).
cell_land([15,8], mountain).
cell_land([15,9], forest).
cell_land([15,10], plain).
cell_land([15,11], plain).
cell_land([15,12], forest).
cell_land([15,13], water).
cell_land([15,14], forest).
cell_land([15,15], plain).
cell_land([15,16], plain).
cell_land([15,17], forest).
cell_land([15,18], forest).
cell_land([15,19], forest).
cell_land([15,20], forest).
cell_land([15,21], forest).
cell_land([15,22], forest).
cell_land([15,23], forest).
cell_land([15,24], forest).
cell_land([15,25], forest).
cell_land([15,26], forest).
cell_land([15,27], forest).
cell_land([15,28], forest).
cell_land([15,29], forest).
cell_land([15,30], forest).


cell_land([16,1], forest).
cell_land([16,2], plain).
cell_land([16,3], plain).
cell_land([16,4], plain).
cell_land([16,5], plain).
cell_land([16,6], plain).
cell_land([16,7], water).
cell_land([16,8], plain).
cell_land([16,9], plain).
cell_land([16,10], plain).
cell_land([16,11], plain).
cell_land([16,12], plain).
cell_land([16,13], plain).
cell_land([16,14], plain).
cell_land([16,15], plain).
cell_land([16,16], plain).
cell_land([16,17], plain).
cell_land([16,18], plain).
cell_land([16,19], plain).
cell_land([16,20], forest).
cell_land([16,21], plain).
cell_land([16,22], plain).
cell_land([16,23], plain).
cell_land([16,24], plain).
cell_land([16,25], plain).
cell_land([16,26], plain).
cell_land([16,27], plain).
cell_land([16,28], plain).
cell_land([16,29], plain).
cell_land([16,30], forest).

cell_land([17,1], forest).
cell_land([17,2], plain).
cell_land([17,3], plain).
cell_land([17,4], plain).
cell_land([17,5], plain).
cell_land([17,6], plain).
cell_land([17,7], plain).
cell_land([17,8], plain).
cell_land([17,9], plain).
cell_land([17,10], plain).
cell_land([17,11], plain).
cell_land([17,12], plain).
cell_land([17,13], plain).
cell_land([17,14], plain).
cell_land([17,15], plain).
cell_land([17,16], plain).
cell_land([17,17], plain).
cell_land([17,18], plain).
cell_land([17,19], plain).
cell_land([17,20], forest).
cell_land([17,21], plain).
cell_land([17,22], plain).
cell_land([17,23], forest).
cell_land([17,24], forest).
cell_land([17,25], forest).
cell_land([17,26], forest).
cell_land([17,27], forest).
cell_land([17,28], plain).
cell_land([17,29], plain).
cell_land([17,30], forest).

cell_land([18,1], forest).
cell_land([18,2], plain).
cell_land([18,3], plain).
cell_land([18,4], plain).
cell_land([18,5], plain).
cell_land([18,6], plain).
cell_land([18,7], water).
cell_land([18,8], plain).
cell_land([18,9], plain).
cell_land([18,10], plain).
cell_land([18,11], plain).
cell_land([18,12], plain).
cell_land([18,13], water).
cell_land([18,14], plain).
cell_land([18,15], plain).
cell_land([18,16], plain).
cell_land([18,17], plain).
cell_land([18,18], plain).
cell_land([18,19], plain).
cell_land([18,20], forest).
cell_land([18,21], plain).
cell_land([18,22], forest).
cell_land([18,23], plain).
cell_land([18,24], plain).
cell_land([18,25], plain).
cell_land([18,26], plain).
cell_land([18,27], plain).
cell_land([18,28], forest).
cell_land([18,29], plain).
cell_land([18,30], forest).

cell_land([19,1], forest).
cell_land([19,2], plain).
cell_land([19,3], plain).
cell_land([19,4], plain).
cell_land([19,5], plain).
cell_land([19,6], water).
cell_land([19,7], water).
cell_land([19,8], plain).
cell_land([19,9], plain).
cell_land([19,10], plain).
cell_land([19,11], plain).
cell_land([19,12], plain).
cell_land([19,13], water).
cell_land([19,14], water).
cell_land([19,15], plain).
cell_land([19,16], plain).
cell_land([19,17], plain).
cell_land([19,18], plain).
cell_land([19,19], forest).
cell_land([19,20], plain).
cell_land([19,21], plain).
cell_land([19,22], forest).
cell_land([19,23], plain).
cell_land([19,24], forest).
cell_land([19,25], forest).
cell_land([19,26], forest).
cell_land([19,27], plain).
cell_land([19,28], mountain).
cell_land([19,29], plain).
cell_land([19,30], forest).

cell_land([20,1], water).
cell_land([20,2], water).
cell_land([20,3], water).
cell_land([20,4], water).
cell_land([20,5], plain).
cell_land([20,6], water).
cell_land([20,7], plain).
cell_land([20,8], plain).
cell_land([20,9], plain).
cell_land([20,10], plain).
cell_land([20,11], plain).
cell_land([20,12], plain).
cell_land([20,13], plain).
cell_land([20,14], water).
cell_land([20,15], plain).
cell_land([20,16], plain).
cell_land([20,17], plain).
cell_land([20,18], plain).
cell_land([20,19], plain).
cell_land([20,20], plain).
cell_land([20,21], forest).
cell_land([20,22], plain).
cell_land([20,23], plain).
cell_land([20,24], plain).
cell_land([20,25], plain).
cell_land([20,26], forest).
cell_land([20,27], plain).
cell_land([20,28], forest).
cell_land([20,29], plain).
cell_land([20,30], forest).


cell_land([21,1], forest).
cell_land([21,2], forest).
cell_land([21,3], forest).
cell_land([21,4], forest).
cell_land([21,5], forest).
cell_land([21,6], forest).
cell_land([21,7], forest).
cell_land([21,8], forest).
cell_land([21,9], forest).
cell_land([21,10], forest).
cell_land([21,11], forest).
cell_land([21,12], forest).
cell_land([21,13], forest).
cell_land([21,14], forest).
cell_land([21,15], forest).
cell_land([21,16], forest).
cell_land([21,17], forest).
cell_land([21,18], forest).
cell_land([21,19], water).
cell_land([21,20], forest).
cell_land([21,21], forest).
cell_land([21,22], forest).
cell_land([21,23], forest).
cell_land([21,24], forest).
cell_land([21,25], forest).
cell_land([21,26], forest).
cell_land([21,27], forest).
cell_land([21,28], forest).
cell_land([21,29], forest).
cell_land([21,30], forest).




% object_at(Object, Pos)

% Object/Content
%
% [ObjType, ObjName, ObjDescription]

:- dynamic at/2, entity_descr/2.



at([treasure, t1], [5,7]).
entity_descr([treasure, t1], []).

at([treasure, t2], [5,4]).
entity_descr([treasure, t2], []).

at([treasure, t3], [5,4]).
entity_descr([treasure, t3], []).

at([treasure, t4], [7,9]).
entity_descr([treasure, t4], []).

at([treasure, t5], [2,9]).
entity_descr([treasure, t5], []).

at([treasure, t6], [9, 3]).
entity_descr([treasure, t6], []).

at([treasure, t7], [2,19]).
entity_descr([treasure, t7], []).

at([treasure, t8], [5,16]).
entity_descr([treasure, t8], []).

at([treasure, t9], [9,15]).
entity_descr([treasure, t9], []).

at([treasure, ta], [9,19]).
entity_descr([treasure, ta], []).

at([treasure, tb], [9,19]).
entity_descr([treasure, tb], []).

at([treasure, tc], [14,19]).
entity_descr([treasure, tc], []).

at([treasure, td], [13,10]).
entity_descr([treasure, td], []).

at([treasure, tf], [8,7]).
entity_descr([treasure, tf], []).


at([treasure, tp], [4,25]).
entity_descr([treasure, tp], []).
at([treasure, tq], [4,25]).
entity_descr([treasure, tq], []).




at([hostel, h1], [12,5]).
entity_descr([hostel, h1], []).


at([hostel, h2], [2,9]).
entity_descr([hostel, h2], []).

at([hostel, h3], [10,13]).
entity_descr([hostel, h3], []).

%at([hostel, h4], [24,8]).
%entity_descr([hostel, h4], []).


at([opening_potion, op1], [4,8]).
entity_descr([opening_potion, op1], []).

at([opening_potion, op2], [16,19]).
entity_descr([opening_potion, op2], []).

at([opening_potion, op3], [19,6]).
entity_descr([opening_potion, op3], []).

at([opening_potion, op4], [7,19]).
entity_descr([opening_potion, op4], []).

at([opening_potion, op5], [2,2]).
entity_descr([opening_potion, op5], []).

%at([opening_potion, op6], [24,12]).
%entity_descr([opening_potion, op6], []).





at([sleep_potion, sp1], [8,7]).
entity_descr([sleep_potion, sp1], []).

at([sleep_potion, sp2], [11,21]).
entity_descr([sleep_potion, sp2], []).

at([sleep_potion, sp3], [11,21]).
entity_descr([sleep_potion, sp3], []).

at([sleep_potion, sp4], [11,21]).
entity_descr([sleep_potion, sp4], []).


%at([sleep_potion, sp5], [24,2]).
%entity_descr([sleep_potion, sp5], []).

%at([sleep_potion, sp6], [24,2]).
%entity_descr([sleep_potion, sp6], []).

at([sleep_potion, sp7], [18,27]).
entity_descr([sleep_potion, sp7], []).


at([grave, g1], [7,11]).
entity_descr([grave, g1], [[open, false]]).
has([grave, g1], [treasure, tn]).
entity_descr([treasure, tn], []).

at([grave, g2], [14,12]).
entity_descr([grave, g2], [[open, false]]).
has([grave, g2], [treasure, to]).
entity_descr([treasure, to], []).


at([grave, g3], [2,7]).
entity_descr([grave, g3], [[open, false]]).
has([grave, g3], [treasure, te]).
entity_descr([treasure, te], []).

at([grave, g4], [20,25]).
entity_descr([grave, g4], [[open, false]]).
has([grave, g4], [treasure, ti]).
entity_descr([treasure, ti], []).
has([grave, g4], [treasure, tj]).
entity_descr([treasure, tj], []).
has([grave, g4], [treasure, tk]).
entity_descr([treasure, tk], []).


at([grave, g5], [2,21]).
entity_descr([grave, g5], [[open, false]]).
has([grave, g5], [treasure, tm]).
entity_descr([treasure, tm], []).



% Cambiar treasure p1 por parchment.
% Cambier hostel g1 por grave.

/*
at([treasure, p1], [6,7]).
entity_descr([treasure, p1], [[goals, [at(Obj, [5,5])]], [facts, [has(Grave, Obj), entity_descr(Obj, Descr)]]]):-
	Obj = [treasure, t4],
	Grave = [hostel, g3],
	has(Grave, [treasure, te]), % si falla se invalida el pergamino
	entity_descr(Grave, Descr).
*/

























