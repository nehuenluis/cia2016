% Things to do:
/*
Hacer una thread que cada 10 segundos, o un poco m�s, elimine los nombres de
los agentes que se desconectaron (elimina todos aquellos cuyos ids no aparecen
en la lista de which_agents).

Cambiar cosa por entidad!!!

Generalizaciones:
- Hacer algo parecido a los atributo climbing y climbing_for, que sea
time executing_action y executing action_for. Adem�s definir un
set_executin_action (en lugar de set_climbing) que lo ponga a ejecutar
la accion, y dependiendo de la misma lo pone el tiempo que corresponda
(en game settings defino para cada accion su costo en tiempo, al igual
que defino su costo en stamina).

- Para ganar uniformidad, representar toda la informaci�n de las cosas
con los predicados has, at y entity_descr, incluso para los agentes (no
creo que esto sea ineficiente). Luego, sea cual sea la propiedad que
tengo que actualizar, de la cosa que sea, uso el predicado
update_property!!! Lo mismo para saber el valor de la propiedad, uso
property_value, o algo as�.
Alternativa (tal vez) m�s eficiente!!!.
Hacer un predicado value(ThingID, PropName, PropValue). Si prolog
hace el hash por los dos primeros argumentos es muy eficiente. Luego
consulto y actualizo propiedades de todas las cosas con predicados
generales!!! Al cargar la grilla tengo que "cargar" las entities que esta
describe (luego de cargarlas de hecho puedo eliminarlas).
Si quiero agregar algo din�micamente no hay problema, puedo usar la
representaci�n actual, y cargarlo, o directamente crear la cosa con
la nueva representaci�n.
Los items que siguen se aplican sin diferencia.
POR AHORA uniform� los m�todos de acceso y actualizaci�n (value/3 y
update/5), pero dej� la representaci�n con ag_attr para agentes y con
entity_descr para el resto de las cosas. Lo bueno es que si luego
uniformo la representaci�n, luego solo hay que modificar los predicados
de acceso y actualizaci�n!!!

- Respecto a la visibilidad, puedo hacerlo m�s general. Deber�a poder
especificar que una cierta propiedad de un tipo de entidad dada es
visible para un tipo de agente dado. MMM, ver este �ltimo parametro.
Tal vez no tenga tanto sentido, y todos los tipos de agentes deber�an
percibir lo mismo por ahora. Deber�a analizar bien que no se haga muy
ineficiente el tema de subir por la jerarqu�a de herencia tambi�n con
este par�metro. Implementar bien!!! Adem�s no estoy seguro que se
aplique naturalmente la herencia ac�.

- Finalmente, la entity_descr que se env�a a trav�s de la percepci�n se
calcular� en base a la entity_descr del entorno, simplemente filtrando
aquellas propiedades visibles de acuerdo a la visibilidad establecida.
*/

:- dynamic perc_request_from/1, perc_for/2, update_later/3.

:- dynamic turn/1, forbidden_entry/3, ag_attr/3, at/2, has/2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Java communication dynamic predicates

:- dynamic j_agent_action/2, j_agent_becomes_unconscious/1, j_drop_all/1, j_entity_removed/2.
:- dynamic j_update/1.
:- dynamic holds_from_to/3.
%           j_agent_moved_to/2, j_agent_turned_dir/2,
%           j_agent_conscious/1, j_agent_unconscious/1,
%           j_agent_picked_up/2, j_agent_dropped/2,
%           j_agent_attacked/2.

:- dynamic id/1, transitable_poss/1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


turn(0).

:- consult(comarca), consult(game_settings), consult(env_primitives), consult(extras).


env_setup:-
        findall(Pos, (cell_land(Pos, plain) ; cell_land(Pos, mountain)), Poss),
        assert(transitable_poss(Poss)),
        set_holds_from(generation_time, 15).

/*
env_setup:-
        AgentsPath = 'C:\\Luceo\\Universidad\\Docencia\\IA\\2011\\Proyectos-2011\\Pr-1-2011-Rol_Game(planning)\\Env_Implementation\\GameInterface\\Agents\\',
        concat(AgentsPath, 'dragon.pl', DragonPath),
        %process_create('c:\\Program Files\\pl\\bin\\swipl-win.exe',[DragonPath],[stdin(std),stdout(std),stderr(std)]).
        process_create('c:\\Program Files\\pl\\bin\\swipl-win.exe',[],[]).
*/


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Type inheritance hierarchy


is_a_direct(dragon, agent).

is_a_direct(hostel, building).

is_a_direct(grave, building).

is_a_direct(treasure, object).

is_a_direct(potion, object).

is_a_direct(opening_potion, potion).

is_a_direct(sleep_potion, potion).


%is_a(Type, AncestorType):- is_a_direct(Type, AncestorType).
is_a(Type, Type).

is_a(Type, AncestorType):- is_a_direct(Type, ParentType), is_a(ParentType, AncestorType).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% type_property(Type, Property).
%%
%% A�n no lo us�

type_property_direct(treasure, pickable).


type_property(Type, Property):- is_a(Type, AncestorType),
                                type_property_direct(AncestorType, Property).

%type_property(Type, Property):- is_a(Type, AncestorType),
%                                type_property_direct(AncestorType,
%                                Property).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Configuraci�n de la Comarca

% cell_land(Pos, Land)
%
% Land:
%
% plain
% water
% mountain
% forest

cell_content(Pos, Content):- findall(Thing, at(Thing, Pos), Content).

% at(Thing, Pos)

at([AgType, AgName], Pos):- value([AgType, AgName], pos, Pos), is_a(AgType, agent).

%at([BType, BName], Pos):- building(BType, BName, Pos, _BDescr).

% at([ObjType, ObjName], Pos):- object_at([ObjType, ObjName, _ObjDescr],
% Pos).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Agents info:
%
% Attributes
% ag_attr(AgName, AttrName, Value)
%
% Attributes: max_stamina, fight_skill, load_capacity (skill attributes)
%             pos, dir, (current) stamina, load, resting, resting_for,
%             unconscious, unconscious_for, climbing, climbing_for
%
% Inventory
% has(Ag, [ObjType, ObjName, ObjDescription]) % o carry/carries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- dynamic ag_attr/3, has/2.

% Attribute resting (derived attribute)
%ag_attr(AgName, resting, false):- ag_attr(AgName, resting_for, -1), !.
%ag_attr(_AgName, resting, true).


% Attribute unconscious (derived attribute)
ag_attr(AgID, unconscious, false):- ag_attr(AgID, unconscious_for, -1).
%ag_attr(_AgName, unconscious, true).
ag_attr(AgID, unconscious, true):- ag_attr(AgID, unconscious_for, UF),UF >= 0.

% Attribute climbing (derived attribute)
ag_attr(AgID, climbing, false):- ag_attr(AgID, climbing_for, -1).
ag_attr(AgID, climbing, true):- ag_attr(AgID, climbing_for, CF), CF >= 0.


% Attribute current_turn_action (derived attribute)

ag_attr(AgID, previous_turn_action, Action):-
                          ag_attr(AgID, last_action, [Last_action, ActionTurn]),
                          turn(CurrTurn),
                          ( CurrTurn is ActionTurn + 1, % CurrTurn es el turno siguiente a ActionTurn
                            Action = Last_action
                            ;
                            not(CurrTurn is ActionTurn + 1),
                            Action = none
                          ).

ag_attr(AgID, attacked_by, Attackers):- ag_attr(AgID, attacked_by_possibly_empty, Attackers).
                                          %Attackers \= [].

ag_attr(AgID, harmed_by, Attackers):- ag_attr(AgID, harmed_by_possibly_empty, Attackers).
                                          %Attackers \= [].

ag_attr(AgID, fight_skill, FightSkill):- ag_attr(AgID, attacks_won, AttacksWon),
                                           fight_skill_function(AttacksWon, FightSkill).

% Para que no se calcule cada vez que se consulta el atributo, podr�a incluirse un atributo
% fight_skill_computed, y que fight_skill consulte de ah� mientras ninguno de los valores
% empleados para calcular fight skill haya cambiado. Ver como determinarlo, pero me parece que no
% conviene.

ag_attr(AgID, max_stamina, MaxStamina):- ag_attr(AgID, num_of_training_actions, TrainingActions),
                                         AgID = [AgType, _],
                                         max_stamina_function(AgType, TrainingActions, MaxStamina).

ag_attr(AgID, spelled, true):- holds(spelled(AgID)).
ag_attr(AgID, spelled, false):- not(holds(spelled(AgID))).

%ag_attr(_AgName, previous_turn_action, none).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ag_registration_setup(+AgName)
%%
%% Initializes agent info

ag_registration_setup(AgID):-
%          assert(ag_attr(AgName, max_stamina, MaxSt)),
%          initial_fight_skill(InitFS),
%          assert(ag_attr(AgName, fight_skill, InitFS)),
%         assert(ag_attr(AgName, load_capacity, 10)),
%          ags_starting_pos(StartingPos),

          writeln('Entro al ag_registration_setup'),

          assert(ag_attr(AgID, attacks_won, 0)),
          assert(ag_attr(AgID, num_of_training_actions, 0)),

          (   starting_conditions_available(AgID, Conditions),
              member([starting_pos, StartingPos], Conditions),
              member([starting_dir, StartingDir], Conditions)
              %, AgID = [dragon, _] % Solo se da lugar al pedido si se trata de un dragon
              ;
              ag_starting_position([StartingPos, StartingDir]),
              writeln('1')
          ),
          assert(ag_attr(AgID, pos, StartingPos)), % set Agent position
          assert(ag_attr(AgID, dir, StartingDir)), % set Agent direction
          ag_attr(AgID, max_stamina, MaxSt),
          writeln('2'),
          assert(ag_attr(AgID, stamina, MaxSt)), % set current Agent stamina
          assert(ag_attr(AgID, unconscious_for, -1)), % set Agent "conscious state"
          assert(ag_attr(AgID, climbing_for, -1)), % set Agent "climbing state"
          %assert(ag_attr(AgName, last_action_done, none)),
          %assert(ag_attr(AgName, last_action_turn, 0)),
          assert(ag_attr(AgID, last_action, [none, 0])),
          assert(ag_attr(AgID, attacked_by_possibly_empty, [])),
          assert(ag_attr(AgID, harmed_by_possibly_empty, [])),


          ag_attr(AgID, pos, AgPos),
          ag_attr(AgID, dir, AgDir),

          writeln('3'),

          assert(j_new_agent(AgID, AgPos, AgDir)),
          writeln('Termino el ag_registration_setup').



n_of_connected_ags(NCA):- which_agents(Agents),
                          length(Agents, NCA).

ag_starting_position(SPos):- n_of_connected_ags(NCA),
                             ags_starting_positions(StartingPositions),
                             length(StartingPositions, NOfStartingPositions),
                             InitialPosIndex is ((NCA - 1) mod NOfStartingPositions) + 1,
                             element_at(InitialPosIndex, StartingPositions, SPos).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% update(+Ag, +Attr, _CurrValue, _NewValue, +Where)
%
% ej: update(Ag, stamina, CurrValue, NewValue, NewValue is CurrValue + 1)
%     update(Ag, dir, CurrValue, NewValue, next_90_clockwise(CurrValue, NewValue))
%     update(Ag, pos, _CurrValue, [1,1], true)
%
% ACLARACI�N: la meta Where no debe fallar.

update(Entity, Attr, CurrValue, NewValue, Where):-
                Entity = [Type, _Name], write(Entity),
                is_a(Type, agent),
                retract(ag_attr(Entity, Attr, CurrValue)), write(ag_attr(Entity, Attr, CurrValue)),
                write(Where),
                call(Where),        % Cuidado!!! Deber�a asegurarme que el where no falle!!!
                write('paso el where???'),
                write(ag_attr(Entity, Attr, NewValue)),nl,
                assert(ag_attr(Entity, Attr, NewValue)).


update(Entity, Prop, CurrValue, NewValue, Where):-
                Entity = [Type, _Name],
                not(is_a(Type, agent)),
                entity_descr(Entity, Descr),
                replace([Prop, CurrValue], [Prop, NewValue], Descr, NewDescr),
                call(Where), % Cuidado!!! Deber�a asegurarme que el where no falle!
                retract(entity_descr(Entity, Descr)),
                assert(entity_descr(Entity, NewDescr)).


% update belief

update_b(Entity, Attr, CurrValue, NewValue, Where):-
        update(Entity, Attr, CurrValue, NewValue, Where).

assert_b(Belief):-
        assert(Belief),
        assert(j_update(assert(Belief))).

retract_b(Belief):-
        retract(Belief),
        assert(j_update(retract(Belief))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% value(+Entity, +Prop, -Value)

value(Entity, Attr, Value):-
      Entity = [Type, _Name],
      is_a(Type, agent),
      ag_attr(Entity, Attr, Value).

value(Entity, Attr, Value):-
      Entity = [Type, _Name],
      not(is_a(Type, agent)),
      entity_descr(Entity, Descr),
      member([Attr, Value], Descr).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
property(Thing, Prop, Value):-
        entity_descr(Thing, Descr),
        member([Prop, Value], Descr).


update_prop(Thing, Prop, CurrValue, NewValue, Where):-
                entity_descr(Thing, Descr),
                replace([Prop, CurrValue], [Prop, NewValue], Descr, NewDescr),
                call(Where), % Cuidado!!! Deber�a asegurarme que el where no falle!
                retract(entity_descr(Thing, Descr)),
                assert(entity_descr(Thing, NewDescr)).

*/

%% The Game (Environment)

run:- run_one_turn,!,
      run.



run_one_turn:-
      %sleep(aire) %cuidado! asegurarme que igual reciba mensajes.
      dynamic_env_update,

      %display_env, nl,
      turn(T), write(T), writeln('>'),

      give_requested_percs, %give requested percepts
      %write('give_requested_percs finaliz� con exito'), nl,

      post_perc_dynamic_env_update,

      time_to_think(TimeToThink),
      sleep(TimeToThink),
      excecute_available_actions,
      nl,writeln('---------------------------').


dynamic_env_update:- retract(turn(PrevT)),
                     CurrentT is PrevT + 1,
                     assert(turn(CurrentT)),
                     %retractall(ag_attr(AgName, resting, 0)),
                     %forall(ag_attr(AgName, resting, true),
                     %(retract(ag_attr(AgName, resting_for, N)), PredN is N-1, assert(ag_attr(AgName, resting_for, PredN)))),
                     forall(value(AgID, climbing, true),
                     update(AgID, climbing_for, CurrV, NewV, NewV is CurrV - 1)),
                     AgID = [AgType, _AgName],
                     forall(value(AgID, unconscious_for, 0),
                     (update(AgID, stamina, _CurrV, NewV, wake_up_stamina(AgType, NewV)))),
                     % Les da stamina a los agentes que se despiertan en este turno.
                     forall(value(AgID, unconscious, true),
                     update(AgID, unconscious_for, CurrV, NewV, NewV is CurrV - 1)),
                     forall((at([hostel, HName], Pos),
                             value(AgID, pos, Pos),
                             value(AgID, stamina, St),
                             value(AgID, max_stamina, MaxSt)),

                             (St >= MaxSt-1,

                             /* Ya no expulso al caballero de la posada. Esto los desorientaba un poco.
                              ady_at_cardinal(Pos, _Card, AdyPos),
                              not(cell_land(AdyPos, forest)),
                              not(cell_land(AdyPos, water)),
                              update(AgID, pos, _CurrPos, AdyPos, true),
                              assert(j_agent_action(AgID, move_to(AdyPos))),*/

                              set_forbidden_entry(AgID, [hostel, HName], true)
                             /*
                              turn(Turn),
                              forbidden_entry_time(FET),
                              UntilTurn is Turn + FET,
                              assert(forbidden_entry(AgID, [hostel, HName], UntilTurn))
                             */

                                 ;

                              (St < MaxSt, %podr�a quitar esta condici�n
                                     (   can_enter(AgID, [hostel, HName]),
                                         hostel_recovery_rate(RR),
                                         update(AgID, stamina, _CurrSt, NewSt, NewSt is min(St+RR, MaxSt))
                                         % decirle a java que refresque el info panel. Ver como.
                                     ;
                                         true
                                     )
                              )
                             )
                           ),

                      forall((forbidden_entry(AgID, HID, UntilTurn), turn(Turn), Turn > UntilTurn),
                             set_forbidden_entry(AgID, HID, false)),

                      implies(holds(generation_time), (turn(Turn), From is Turn + 20, set_holds_from(generation_time, From), spontaneous_generation)).



spontaneous_generation:-
        implies((amountOf(potion, C), desired_amount_of_potions(D), C < D), (R is D - C, repeat_n(generate(potion), R))).

amountOf(Type, Amount):-
        findall(EType,
                ((at([EType, _EName], _) ; has(_Entity, [EType, _EName])), is_a(EType, Type)),
               ExistingTypes),
        length(ExistingTypes, Amount).





generate(potion):-
        transitable_poss(Poss),
        random_member(Pos, Poss),
        random_member(PotionType, [sleep_potion]),
        writeln(generating(potion, Pos)),
        generateId(Id),
        assert_b(at([PotionType, Id], Pos)),
        assert_b(entity_descr([PotionType, Id], [])),
        writeln(Id).



id(0).

generateId(id(X)):-
        retract(id(X)),
        X1 is X+1,
        assert(id(X1)).


post_perc_dynamic_env_update:- forall(value(AgID, attacked_by_possibly_empty, Attackers),
                                      update(AgID, attacked_by_possibly_empty, Attackers, [], true)),
                               forall(value(AgID, harmed_by_possibly_empty, Attackers),
                                      update(AgID, harmed_by_possibly_empty, Attackers, [], true)).
                               %retractall(value(AgName, attacked_by, _Attackers)),
                               %retractall(value(AgName, harmed_by, _Attackers)).


% set_forbidden_entry(+AgID, +HostelID, +Forbidden)

set_forbidden_entry(AgID, HostelID, true):-
        turn(Turn),
        forbidden_entry_time(FET),
        UntilTurn is Turn + FET,
        retractall(forbidden_entry(AgID, HostelID, _UntilTurn)), % Para evitar, por error de programaci�n, m�ltiples forbidden_entry
        assert(forbidden_entry(AgID, HostelID, UntilTurn)).


set_forbidden_entry(AgID, HostelID, false):-
        retractall(forbidden_entry(AgID, HostelID, _UntilTurn)).


give_requested_percs:-
                   registered_agents(Ags),
                   forall((member(Ag, Ags), not(unconscious(Ag)), not(climbing(Ag)), not(holds(executing_action(Ag, _Action))), perc_request_available(Ag)), assert(perc_request_from(Ag))),
                   % el not(unconscious(Ag)) hace que si un dado Ag est� unconscious, entonces no se le recibir� la solicitud
                   % de percepci�n, y por lo tanto quedar� bloqueado hasta que vuelva a estar consciente.
                   %
                   %forall(perc_request_from(Ag), (generate_perc(Ag, Perc), retract(perc_request_from(Ag)), assert(perc_for(Ag, Perc)))),
                   % Version con handler
                   forall(perc_request_from(Ag),
                   (
                      if_fails_do(generate_perc(Ag, Perc), (writeln('***fallo generate_perc for***'), write(Ag), nl)),
                      retract(perc_request_from(Ag)),
                      assert(perc_for(Ag, Perc))
                   )),
                   %
                   %El retract(perc_request_from(Ag)) lo hago luego del generate_perc(Ag, Perc) por si este �ltimo falla, as� no me olvido
                   %que todav�a le debo la percepci�n al agente.
                   forall(perc_for(Ag, Perc), (give_percept(Perc, Ag), retract(perc_for(Ag, Perc)))).

% Considerar la alternativa de usar un hilo que constantemenre reciba solicitudes de percepciones
% y las responda. Usar sem�foro para lograr atomicidad de la operaci�n de update_game_state!!
% Con esto me evito el sleep(aire)!!!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perceptions
%
% [Turn, AtSigh, Attrs, Inventory]
%
% Turn/Time
%
% AtSigh = [ [Pos, Land, Objects], ... ]
%
% where  Pos = [X,Y]
%        Land = plain / forest / water / mountain
%        Objects = [ [Object, Name, Description], ... ]
%
%        where Object = treasure / hostel / agent
%
% Attrs = [ [AttrName, Value/Description], ... ]
%
% Inventory = [ [Object, Name, Description], ... ]
/*
old_generate_perc(Ag, [Turn, Vision, Attrs, Inventory]):-
                  turn(Turn),
                  generate_vision(Ag, Vision), %write(Vision), nl,
                  generate_attrs(Ag, Attrs), %write(Attrs), nl,
                  generate_inventory(Ag, Inventory). %write(Inventory), nl.
*/

generate_perc(Ag, [turn(Turn)|Relations]):-
                    turn(Turn),
                    value(Ag, pos, AgPos),
                    value(Ag, dir, AgDir),
                    findall(land(Pos, CellLand), (pos_at_sight(AgPos, AgDir, Pos), cell_land(Pos, CellLand)), Land),
                    findall(Relation, (at(ThingID, Pos),
                                       pos_at_sight(AgPos, AgDir, Pos),
                                       holds_relation(Ag, Relation, ThingID)),
                             DynamicRelations),
                    append(Land, DynamicRelations, Relations).


% Posici�n del propio agente

holds_relation(AgID, at([AgType, me], Pos), AgID):-
        AgID = [AgType, _AgName],
        at(AgID, Pos).


% Posici�n de otros agentes y objetos

holds_relation(AgID, at(ThingID, Pos), ThingID):-
        ThingID \= AgID,
        at(ThingID, Pos).

% Descripci�n del propio agente

holds_relation(AgID, entity_descr([AgType, me], SensableDescr), AgID):-
        AgID = [AgType, _AgName],
        generate_attrs(AgID, SensableDescr).

% Objetos en el inventario del propio agente

holds_relation(AgID, has([AgType, me], ObjID), AgID):-
        AgID = [AgType, _AgName],
        has(AgID, ObjID).

% Descripci�n de los objetos en el inventario del propio agente

holds_relation(AgID, entity_descr(ObjID, ObjDescr), AgID):-
        has(AgID, ObjID),
        entity_descr(ObjID, ObjDescr).

% Descripci�n de otros agentes y objetos

holds_relation(AgID, entity_descr(ThingID, VisibleDescr), ThingID):-
        ThingID \= AgID,
        visible_descr(ThingID, VisibleDescr).


% Objetos en el inventario de otros agentes o dentro de buildings

holds_relation(AgID, has(ThingID, ObjId), ThingID):-
        ThingID \= AgID,
        has(ThingID, ObjId).


% Descripci�n de los objetos en el inventario de otros agentes o dentro
% de buildings

holds_relation(AgID, entity_descr(ObjID, ObjDescr), ThingID):-
        ThingID \= AgID,
        has(ThingID, ObjID),
        entity_descr(ObjID, ObjDescr).



/*

generate_perc_relations(Ag, Relations):-
                    value(Ag, pos, AgPos),
                    value(Ag, dir, AgDir),
                    findall(Relation, (pos_at_sight(AgPos, AgDir, Pos),
                                       holds_relation(Ag, Relation, Pos)),
                             Relations).


holds_relation(_Ag, land(Pos, Land), Pos):-
        cell_land(Pos, Land).

holds_relation(_Ag, at(ThingID, Pos), Pos):-
        at(ThingID, Pos).

holds_relation(Ag, entity_descr([agent, me], SensableDescr), Pos):-
        at([agent, Ag], Pos),
        %sensable_descr([agent, me], SensableDescr).
        generate_attrs(Ag, SensableDescr).

holds_relation(Ag, entity_descr(ThingID, VisibleDescr), Pos):-
        at(ThingID, Pos),
        ThingID \= [agent, Ag],
        visible_descr(ThingID, VisibleDescr).

% obtiene la descripci�n de un objeto cargado por un agente que se
% encuentra en la posici�n dada.
%
% Descripci�n de los objetos en el inventario del propio agente

holds_relation(Ag, entity_descr([ObjType, ObjName], ObjDescr), Pos):-
        at([agent, Ag], Pos),
        has(Ag, Object),
        Object = [ObjType, ObjName, ObjDescr].

% Descripci�n de los objetos en el inventario de otros agentes

holds_relation(Ag, entity_descr([ObjType, ObjName], ObjDescr), Pos):-
        at([agent, AgName], Pos),
        AgName \= Ag,
        has(AgName, Object),
        Object = [ObjType, ObjName, ObjDescr].

*/

% visible_descr([ThingType, ThingName], VisibleDescr))

visible_descr(AgID, VisibleDescr):-
        AgID = [AgType, _AgName],
        is_a(AgType, agent),
        findall([AttrName, AttrVal], (visible_attr(AgType, AttrName), value(AgID, AttrName, AttrVal)), VisibleDescr).

visible_descr([hostel, HName], HDescr):-
        entity_descr([hostel, HName], HStaticDescr),
        findall([AgID, UntilTurn], forbidden_entry(AgID, [hostel, HName], UntilTurn), Bans),
        HDescr = [[forbidden_entry, Bans]|HStaticDescr].

visible_descr(ThingID, ThingDescr):-
        ThingID \= [hostel, _Hname],
        entity_descr(ThingID, ThingDescr).

% visible_attr(Type, Attr)
%
% establece que el attributo Attr del tipo Type es visible para
% los agentes.
% Aclaraci�n: si pueden existir distintos tipos de agentes con
% distintas capacidades de visi�n, entonces tengo que agragar un
% argumento AgentType al predicado indicando el tipo del agente
% que puede ver el atributo en cuesti�n (del tipo considerado).
% Sobre este argumento tambi�n debo aplicar la herencia.

% MMMM, con los ! no puedo llamarlo con el atributo sin instanciar.

% ESTOS SON LOS ATRIBUTOS QUE UN AGENTE VA A VER DEL RESTO DE LOS
% AGENTES

visible_attr_direct(agent, dir).

%visible_attr_direct(agent, stamina).

%visible_attr_direct(agent, max_stamina).

%visible_attr_direct(agent, fight_skill).

visible_attr_direct(agent, unconscious). %:- !.

visible_attr_direct(agent, previous_turn_action). %:- !.

visible_attr_direct(agent, attacked_by). %:- !.

visible_attr_direct(agent, harmed_by). %:- !.

%visible_attr_direct(agent, spelled).

visible_attr(Type, Attr):- is_a(Type, AncestorType),
                           visible_attr_direct(AncestorType, Attr).



generate_attrs(AgID, Attrs):-
        AgID = [AgType, _AgName],
        findall([AttrName, Value], (sensable_attr(AgType, AttrName), value(AgID, AttrName, Value)), Attrs).


% ESTOS SON LOS ATRIBUTOS QUE UN AGENTE VA A VER DE SI MISMO (ADEMAS DE
% LOS VISIBLES --- QUE SE VEN PARA TODO AGENTE)
% sensable_attr(AgType, AttrName)

sensable_attr_direct(agent, pos). % VEO SOLO MI POSICION, LA DE LOS OTROS AGENTES NO PORQUE APARECE EN LOS AT

%sensable_attr_direct(agent, dir).

sensable_attr_direct(agent, stamina).

sensable_attr_direct(agent, max_stamina).

sensable_attr_direct(agent, fight_skill).

sensable_attr(Type, Attr):- is_a(Type, AncestorType),
                                   sensable_attr_direct(AncestorType, Attr).

sensable_attr(Type, Attr):- visible_attr(Type, Attr).



excecute_available_actions:-
                            registered_agents(Ags),
                            write('actions from: '),
                            forall((random_member(Ag, Ags),
                                    %write(Ag), write(' '),
                                    action_available(Action, Ag)), assert(action_from(Action, Ag))),
                            update_game_state.
                            % NO OLVIDARME DE QUITAR TODAS LAS ACCIONES.

% Esta forma de ejecutar las acciones, es decir, se juntan todas las disponibles
% y se ejecutan todos "a la vez", evita que el entorno cambie mientras que un
% agente est� pensando su pr�xima acci�n (por supuesto, salvo que el agente se
% tarde mucho en pensar y comunique su acci�n en el pr�ximo turno).
% Si por el contrario se implementara un esquema donde el entorno est� en todo
% momento sministrando percepciones apenas son demandadas y recorriendo la
% lista de agentes ejecutando las acciones disponibles, entonces los agentes
% m�s r�pidos tienen m�s chance de que el estado del mundo no haya cambiado
% desde que percibieron hasta que ejecutaron la acci�n, mientras que los m�s
% lentos tienen m�s chance de que sus acciones se ejecuten cuando el estado
% del mundo ya cambi� (respecto a la percepci�n que motiv� la acci�n) y tal
% vez ya no sea la acci�n m�s apropiada al momento en que es ejecutada.
% Aunque este enfoque es m�s realista, se aleja del esquema "por turnos"
% resultando m�s complicado. Adem�s existe el riesgo de que una estrategia
% m�s reactiva y simple tenga ventaja sobre una m�s deliberativa, ya que la
% primera permitir� al agente actuar m�s cantidad de veces. No queremos eso!!!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actions
%
% move_fwd
%
% turn(Dir),          where Dir = n / e / s / o.
%
% attack(AgName)      AgName es el nombre de la v�ctima
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% action/4
%
% action(Signature, From, Pre, Effects)
%
% Para especificaci�n de acciones

action(attack(Ag2), AgFrom,

( % Pre
AgFrom \= Ag2,
not(climbing(AgFrom)),
not(unconscious(AgFrom)),
not(climbing(Ag2)), %VER
not(unconscious(Ag2)),
can_attack(AgFrom, Ag2),
Action = attack(Ag2)
),

( % Effects
solve_attack(AgFrom, Ag2),
stamina_cost(attack, C),
time_cost_agent(AgFrom, attack, T),
set_holds_for(executing_action(AgFrom, Action), T),
update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
%assert(j_agent_attacked(Ag1, Ag2))
update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),
update(Ag2, attacked_by_possibly_empty, CurrV, NewV, append(CurrV, [AgFrom], NewV)),
assert(j_agent_action(AgFrom, Action))
% Por ahora, el ataque no cuesta energ�a a quien ataca
)).

% Aclaraci�n: El solve attack no refleja los efectos del ataque hasta que se
% haga el commit. Esto es porque se asume que los ataques se resuelven en
% simult�neo.
%
% Cuastiones a Resolver
%
% Qu� sucede si empatan? (la v�ctima no sufre da�o,
% pero el atacante gana en skill? -> Ver como pongo el >).
%
% si un agente ejecuta move_fwd pero otro lo ataca y lo deja inconsciente,
% se mueve antes de quedar inconsciente o no? Definir!! (Si est� agarrando un
% objeto da igual).

can_attack(Ag1, Ag2):-
                value(Ag1, pos, PosAg1),
                %not(at([hostel, _BName], PosAg1)),
                value(Ag1, dir, DirAg1),
                value(Ag2, pos, PosAg2),
                %not(at([hostel, _BName], PosAg2)),
                %value(Ag2, dir, DirAg2),
                ady_at_cardinal(PosAg1, DirAg1, FrontPos),
                next_90_clockwise(DirAg1, NextDirAg1),
                next_90_clockwise(PrevDirAg1, DirAg1),
                ady_at_cardinal(FrontPos, NextDirAg1, FrontRightPos),
                ady_at_cardinal(FrontPos, PrevDirAg1, FrontLeftPos),
                (PosAg2 = FrontPos ; PosAg2 = FrontRightPos ; PosAg2 = FrontLeftPos ; PosAg2 = PosAg1).


action(take_from(Obj, Container), AgFrom,

( %Pre
not(climbing(AgFrom)),
not(unconscious(AgFrom)),
%pickable(Obj),
value(AgFrom, pos, AgPos),
at(Container, AgPos),
has(Container, Obj),
value(Container, open, true),
Action = take_from(Obj, Container)
%not((value(OtherAg, pos, AgPos), OtherAg \= AgFrom, conscious(OtherAg)))
)
,
( %Effects
assert(has(AgFrom, Obj)),
retract(has(Container, Obj)),
stamina_cost(take_from, C),
time_cost_agent(AgFrom, take_from, T),
set_holds_for(executing_action(AgFrom, Action), T),
update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),

assert(j_agent_action(AgFrom, Action))
)
).



action(pickup(Obj), AgFrom,

( %Pre
not(climbing(AgFrom)),
not(unconscious(AgFrom)),
%pickable(Obj),
value(AgFrom, pos, AgPos),
at(Obj, AgPos),
Action = pickup(Obj)
)
,
( %Effects
assert_b(has(AgFrom, Obj)),
retract_b(at(Obj, AgPos)),
%assert(j_agent_picked_up(AgFrom, ObjName))
time_cost_agent(AgFrom, pickup, T),
set_holds_for(executing_action(AgFrom, Action), T),
stamina_cost(pickup, C),
update_b(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
update_b(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),

assert(j_agent_action(AgFrom, Action))
)
).

% Qu� sucede si dos agentes hacen un pick del mismo objeto al mismo tiempo?
% Puedo tirar un dado para resolver qui�n se lo queda.
%
% Tambi�n puedo pedir como pre que no haya otros agentes conscientes para agarrar
% el objeto (esto motivar� una batalla para quedarse con el tesoro),
% o que no lo agarra ninguno.
% tal vez deba hacer algo parecido a attack!!!
%
% ESTA FUE LA POL�TICA ADOPTADA

action(drop(Obj), AgFrom,

( %Pre
%not(resting(AgFrom)),
% FALTA CONTROLAR QUE EL AGENTE TENGA EL OBJETO QUE DESEA SOLTAR!!!!
not(climbing(AgFrom)),
not(unconscious(AgFrom)),
Action = drop(Obj)
),

( %Effects
 retract(has(AgFrom, Obj)),
 value(AgFrom, pos, Pos),
 assert(at(Obj, Pos)),

 stamina_cost(drop, C),
 time_cost_agent(AgFrom, drop, T),
 set_holds_for(executing_action(AgFrom, Action), T),
 update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
 %write(ObjName),nl,
 %assert(j_agent_dropped(AgFrom, ObjName))
 update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),

 assert(j_agent_action(AgFrom, Action))
)).



action(turn(Dir), AgFrom,

( % Pre
 %not(resting(AgFrom)),
 cadinal_dir(Dir),
 not(climbing(AgFrom)),
 not(unconscious(AgFrom)),
 Action = turn(Dir)
),

( % Effects
 assert(j_agent_action(AgFrom, Action)),

 update(AgFrom, dir, _CurrDir, Dir, true),
 stamina_cost(turn, C),
 time_cost_agent(AgFrom, turn, T),
 set_holds_for(executing_action(AgFrom, Action), T),
 update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
 update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),
 update(AgFrom, num_of_training_actions, CurrTrainigActions, NewTrainigActions, NewTrainigActions is CurrTrainigActions + 1)

)).

% Auxiliary preds

% cadinal_dir(Dir)

cadinal_dir(n).
cadinal_dir(e).
cadinal_dir(s).
cadinal_dir(w).


action(move_fwd, AgFrom,

( % Pre
 %not(resting(AgFrom)),
 not(climbing(AgFrom)),
 not(unconscious(AgFrom)),
 value(AgFrom, pos, Pos),
 value(AgFrom, dir, Dir),
 ady_at_cardinal(Pos, Dir, DestPos),
 not(cell_land(DestPos, water)),
 not(cell_land(DestPos, forest)),
 Action = move_fwd
 %implies(at([hostel, HName], DestPos), can_enter(AgFrom, [hostel, HName]))
),

( % Effects
 update(AgFrom, pos, _CurrPos, DestPos, true),
 ( cell_land(DestPos, mountain),
   stamina_cost(move_fwd_mountain, CM),
   time_cost_agent(AgFrom, move_fwd_mountain, T),
   set_holds_for(executing_action(AgFrom, Action), T),
   update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - CM),
   %set_resting(AgFrom)
   set_climbing(AgFrom)
    ;
   stamina_cost(move_fwd_plain, CP),
   time_cost_agent(AgFrom, move_fwd_plain, T),
   set_holds_for(executing_action(AgFrom, Action), T),
   update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - CP)
 ),
 implies((at([hostel, HName1], Pos), can_enter(AgFrom, [hostel, HName1])), %si ya no puede entrar no tengo que hacer nada.
        (turn(Turn),
         forbidden_entry_time(FET),
         UntilTurn is Turn + FET,
         assert(forbidden_entry(AgFrom, [hostel, HName1], UntilTurn))
        )),
 update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [move_fwd, Turn])),

 assert(j_agent_action(AgFrom, move_to(DestPos))),
 update(AgFrom, num_of_training_actions, CurrTrainigActions, NewTrainigActions, NewTrainigActions is CurrTrainigActions + 1)
)).

% Actions auxiliary predicates

%resting(Ag):- value(Ag, resting, true).

%set_resting(Ag):- mountain_resting_time(RT),
%                  retract(ag_attr(Ag, resting_for, _)),
%                  assert(ag_attr(Ag, resting_for, RT)).


action(cast_spell(open(Grave)), AgFrom,

( % Pre
 not(climbing(AgFrom)),
 not(unconscious(AgFrom)),
 has(AgFrom, [opening_potion, OPName]),
 at(AgFrom, Pos),
 at(Grave, Pos),
 value(Grave, open, false),
 Action = cast_spell(open(Grave))
),

( % Effects
 retract(has(AgFrom, [opening_potion, OPName])),
 update(Grave, open, _, true, true),
 stamina_cost(cast_spell, C),
 update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
 update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),
 time_cost_agent(AgFrom, cast_spell, T),
 set_holds_for(executing_action(AgFrom, Action), T),
 assert(j_entity_removed([opening_potion, OPName], [AgFrom, Action])),
 assert(j_agent_action(AgFrom, Action))
)).

% Si quiero hacer algo del estilo que para abrirlo necesito algo
% espec�fico dependiente del contenedor, entonces hacer cofres y llaves.
% Ver. No lo hago con graves porque sino si alguien se apodera de una
% llave, ning�n otro puede lograr la meta de abrirlo. Ver.


action(cast_spell(sleep(Creature)), AgFrom,

( % Pre
 not(climbing(AgFrom)),
 not(unconscious(AgFrom)),
 has(AgFrom, [sleep_potion, SPName]),
 at(AgFrom, Pos),
 can_attack(AgFrom,Creature),
 not(unconscious(Creature)),
 AgFrom \= Creature,
 Action = cast_spell(sleep(Creature))
),

( % Effects
 retract(has(AgFrom, [sleep_potion, SPName])),
 update(Creature, unconscious_for, _CurrV, UT, unconscious_time(UT)),
 drop_all_objects(Creature),
 stamina_cost(cast_spell, C),
 update(AgFrom, stamina, CurrSt, NewSt, NewSt is CurrSt - C),
 update(AgFrom, last_action, _CurrVal, NewVal, (turn(Turn), NewVal = [Action, Turn])),
 time_cost_agent(AgFrom, cast_spell, T),
 set_holds_for(executing_action(AgFrom, Action), T),
 set_holds_for(spelled(Creature), 30),
 assert(j_agent_action(AgFrom, Action)),
 assert(j_entity_removed([sleep_potion, SPName], [AgFrom, Action])),
 assert(j_agent_becomes_unconscious(Creature))
)).



% replace(+X, +Y, +Set, -NewSet)

replace(X, Y, [X|Xs], [Y|Xs]).

replace(X, Y, [Z|Xs], [Z|NewXs]):-
        X \= Z,
        replace(X, Y, Xs, NewXs).


is_slow([agent, coward]).

time_cost_agent(Agent, Action, Cost):-
        holds(is_slow(Agent)),
        writeln(is_slow(Agent)),
        time_cost(Action, C),
        Cost is (C + 1) * 2 - 1.

time_cost_agent(_Agent, Action, Cost):-
        time_cost(Action, Cost).


climbing(Ag):- value(Ag, climbing, true).

set_climbing(Ag):- update(Ag, climbing_for, _CurrV, CT, climbing_time(CT)).

unconscious(Ag):- value(Ag, unconscious, true).

conscious(Ag):- value(Ag, unconscious, false).

can_enter(AgFrom, [hostel, HName]):- at([hostel, HName], _Pos),
                           not(forbidden_entry(AgFrom, [hostel, HName], _UntilTurn)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%

set_holds_for(Relation, Time):-
        turn(Turn),
        Until is Turn + Time,
        set_holds_until(Relation, Until).

set_holds_until(Relation, Turn):-
        turn(CTurn),
        set_holds_from_to(Relation, CTurn, Turn).

set_holds_from(Relation, From):-
        To = 100000,
        set_holds_from_to(Relation, From, To).

set_holds_from_to(Relation, From, To):-
        From =< To,
        retractall(holds_from_to(Relation, _, _)),
        assert(holds_from_to(Relation, From, To)).

holds(Relation):-
        holds_from_to(Relation, From, To),
        turn(Turn),
        From =< Turn,
        if_fails_do(Turn =< To, retract(holds_from_to(Relation, From, To))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% execute(Action)

execute(Action, AgFrom):- if_fails_do(action(Action, AgFrom, Pre, Effects), (write(AgFrom), write('--> unknown action: '), write(Action), nl)),
                          if_fails_do(Pre, (write(AgFrom), write('--> fallaron las Pre de la acci�n '), write(Action), nl)),
                          if_fails_do(Effects, (write(AgFrom), write('--> fallaron los Efectos de la acci�n '), write(Action), nl)),
                          !. % Este cut es para evitar multiplicidad de soluciones
                             % cuando tanto Pre como Effects se ejecutan con �xito.

execute(_Action, _AgFrom). % El execute siempre tiene �xito.



% IMPLEMENTAR

update_game_state:- forall(retract(action_from(attack(Ag2), Ag)), execute(attack(Ag2), Ag)),
                    %forall(retract(update(Ag, Attr, Quantity)), commit_update(Ag, Attr, Quantity)),
                    commit_updates,
                    forall(retract(action_from(pickup(Obj), Ag)), execute(pickup(Obj), Ag)),
                    % En principio, debo ejecutar todos los pickup antes de los drop, sino un agente
                    % podr�a levantar un objeto en el mismo momento que otro agente lo est� dejando.
                    % (Aunque deber�a adivinar que este lo tiene, y que lo va a dejar efectivamente,
                    % y tendr�a que estar despu�s en el orden de which_agents).
                    forall(retract(action_from(Action, Ag)), execute(Action, Ag)),
                    set_unconscious.
                    % Al hacerlo al final hace que la victima de una ataque logre moverse antes de quedar
                    % inconsciente.

% solve_attack(From, To)
solve_attack(Ag1, Ag2):-
                   value(Ag1, fight_skill, SkillAg1),
                   %if_fails_do(value(Ag2, fight_skill, SkillAg2), write(Ag2)),
                   value(Ag2, fight_skill, SkillAg2),
                   dice_sides(Chance),
                   %if_fails_do(dice(Chance, PlusAg1),write('2')),
                   dice(Chance, PlusAg1),
                   dice(Chance, PlusAg2),
                   AttackPowerAg1 is SkillAg1 + PlusAg1,
                   ResistanceAg2 is SkillAg2 + PlusAg2,
                   AttackPowerAg1 > ResistanceAg2,
                   !,
                   HarmAg2 is AttackPowerAg1 - ResistanceAg2,
                   %HarmAg2 is ceiling((AttackPowerAg1 - ResistanceAg2)*3/4),
                   %assert(update_later(Ag2, stamina, -HarmAg2)),
                   update(Ag2, stamina, CurrSt, NewSt, NewSt is max(CurrSt - HarmAg2, 0)),
                   %fight_skill_reward(SkillReward),
                   %assert(update(Ag1, fight_skill, SkillReward)), %Cuidado!! skill points o skill in battles?
                   assert(update_later(Ag1, attacks_won, 1)),
                   update(Ag2, harmed_by_possibly_empty, CurrV, NewV, append(CurrV, [Ag1], NewV)).
                   %write(Ag1), write(' succesfully attacked '), write(Ag2).

solve_attack(Ag1, Ag2). %:- % assert(update(Ag2, skill, +, Skill)), % Le doy skill points si resiste?
                         %write(Ag2), write(' resisted the attack from '), write(Ag1).

dice(NOfSides, Random):- Random is random(NOfSides).

%commit_update(Ag, Attr, Sign, Quantity). %:- primero definir la estructura de datos para almacenar info
                                          % de los agentes.

commit_updates:- forall(retract(update_later(Ag, Attr, Quantity)),
                        update(Ag, Attr, CurrV, NewV, NewV is max(CurrV + Quantity, 0))).


set_unconscious:- forall((value(Ag, stamina, St), St =< 0, value(Ag, unconscious, false)),
                         (update(Ag, unconscious_for, _CurrV, UT, unconscious_time(UT)),
                         drop_all_objects(Ag),
                         assert(j_agent_becomes_unconscious(Ag)))). % Cuando el agente queda inconsciente, suelta todos los objetos que lleva y estos quedan en el piso.

drop_all_objects(Ag):- value(Ag, pos, Pos), forall(has(Ag, Obj), (retract(has(Ag, Obj)), assert(at(Obj, Pos)))),
                       assert(j_drop_all(Ag)).



%handle_move(...)

%handle_pickup(...)

start :- start_env,
        run.
