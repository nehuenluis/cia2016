%% Player-Agent coward

:- dynamic turn/1, has/2, at/2, entity_descr/2, land/2.

:- consult(ag_primitives), consult(extras_for_agents).

last_perc(none).

run:-
      get_percept(Perc),

      % AGENT CODE
      % (internal state update and action choice)

      update_state(Perc),

      display_ag, nl,

      decide_action(Action),

      do_action(Action),

      run.



update_state(Perc):-

	% El agente olvida todo lo que recordaba
	retractall(turn(_)),
	retractall(at(_,_)),
	retractall(has(_,_)),
	retractall(entity_descr(_,_)),
	retractall(land(_,_)),

	% y recuerda lo que percibi�
	forall(member(Rel, Perc), assert(Rel)).


decide_action(Action):-

      at([agent, me], Pos),
      property([agent, me], dir, Dir),

      ady_at_cardinal(Pos, Dir, PosInFront),
      land(PosInFront, Land),
      (Land = water
           ;
       Land = forest
           ;
       at([EntityType, EntityName], PosInFront),
       is_a(EntityType, agent),
       write('aaaahhhhh, no me ataques '), write(EntityName), write('!!!'),nl %si es un agente, y entonces lo esquiva!!!
       ),
      next_90_clockwise(Dir, DesiredDir),
      Action = turn(DesiredDir),
      write('Action: '), writeln(Action).


decide_action(Action):-

      at([agent, me], MyPos),
      at([treasure, TrName], MyPos),
      write('iuuuujuuuuu, encontre un tesoro conocido como '), write(TrName), write('!!!'),nl,
      write('voy a intentar recogerlo...'),nl,
      Action = pickup([treasure, TrName]),
      write('Action: '), writeln(Action).


decide_action(move_fwd):-
	write('Action: '), writeln(move_fwd).




:- dynamic ag_name/1.


start_ag:- AgID = [agent, coward],
           register_me(AgID, Status),
           !,
           write('REGISTRATION STATUS: '),
           write(Status), nl, nl,
           Status = connected,
           assert(ag_name(coward)),
           run.

s:- start_ag.


start_ag_instance(InstanceID):-
                    AgClassName = coward,
                    AgInstanceName =.. [AgClassName, InstanceID],
                    register_me([agent, AgInstanceName], Status),
                    !,
                    write('REGISTRATION STATUS: '),
                    write(Status), nl, nl,
                    Status = connected,
                    assert(ag_name(AgInstanceName)),
                    run.

si(InstanceID):- start_ag_instance(InstanceID).















