%% Player-Agent dragon

:- consult(ag_primitives), consult(extras_for_agents).

:- dynamic turn/1, at/2, has/2, entity_descr/2, land/2, ag_name/1.


run:-
      get_percept(Perc),

      % AGENT CODE
      % (internal state update and action choice)

      update_beliefs(Perc),

      %display_ag, nl,

      decide_action(Action),

      do_action(Action),

      run.



update_beliefs(Perc):-

        % El agente olvida todo lo que recordaba
        retractall(turn(_)),
        retractall(at(_,_)),
        retractall(has(_,_)),
        retractall(entity_descr(_,_)),
        retractall(land(_,_)),

        % y recuerda lo que percibi�
        forall(member(Rel, Perc), assert(Rel)).



decide_action(Action):-

      at([dragon, me], Pos),
      at([treasure, TrName], Pos),
      write('grrrrrr, mi tesoroooooo '), write(TrName), write('!!!'),nl,
      Action = pickup([treasure, TrName]),
      write('Action: '), writeln(Action).

decide_action(Action):-

      at([dragon, me], Pos),
      at([PotionType, PotionName], Pos),
      is_a(PotionType, potion),
      write('grrrrrr, mi poci�n '), write([PotionType, PotionName]), write('!!!'),nl,
      Action = pickup([PotionType, PotionName]),
      write('Action: '), writeln(Action).


decide_action(Action):-
      at([dragon, me], MyPos),
      at([agent, AgName], Pos),
      property([dragon, me], dir, MyDir),
      pos_in_attack_range(MyPos, MyDir, Pos),
      Action = attack([agent, AgName]),
      write('Action: '), writeln(Action).

decide_action(Action):-
      property([dragon,me],attacked_by,Attackers),
      member([agent,AgName],Attackers),
      at([agent, AgName], PosAg),
      at([dragon, me], MyPos),
      property([dragon, me], dir, MyDir),
      not(pos_in_attack_range(MyPos, MyDir, PosAg)),
      ady_at_cardinal(MyPos,NewDir,PosAg),
      Action = turn(NewDir),
      write('Action: '), writeln(Action).

decide_action(Action):-
      property([dragon,me],attacked_by,Attackers),
      member([agent,AgName],Attackers),
      not(at([agent, AgName], _PosAg)),
      property([dragon, me], dir, MyDir),
      next_90_clockwise(MyDir,NextDir), next_90_clockwise(NextDir,NewDir),
      Action = turn(NewDir),
      write('Action: '), writeln(Action).

decide_action(null_action):- writeln('Action: null_action').



start_ag:- AgID = [dragon, d],
           register_me(AgID, Status),
           !,
           write('REGISTRATION STATUS: '),
           write(Status), nl, nl,
           Status = connected,
           assert(ag_name(d)),
           run.

s:- start_ag.


start_ag_instance(InstanceID):-
                    AgClassName = d,
                    AgInstanceName =.. [AgClassName, InstanceID],
                    register_me([dragon, AgInstanceName], Status),
                    !,
                    write('REGISTRATION STATUS: '),
                    write(Status), nl, nl,
                    Status = connected,
                    assert(ag_name(AgInstanceName)),
                    run.

si(InstanceID):- start_ag_instance(InstanceID).







