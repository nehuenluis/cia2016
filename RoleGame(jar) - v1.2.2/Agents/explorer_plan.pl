%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%	ALGORITMO DE PLANIFICACION
%%	**************************
%%

%%Un Estado es una lista [Pos, Dir] representado la posicion y direccion del agente en un turno
%%Las Metas es una lista de los destinos [Pos, Pos, ...] que se querrian alcanzar
%%El Destino es la meta optima (Pos) que el planificador escogio
%%El Plan es una secuencia de Acciones [move_fwd, move_fwd, turn(e), ...] 
%%que se deben realizar para pasar del EInicial a un estado con el Destino
%%El Costo es la cantidad de turnos que tomara ejecutar el Plan, minimo
%%
%%buscar_plan_desplazamiento(+EInicial, +Metas, -Destino, -Plan, -Costo)
buscar_plan_desplazamiento(EInicial, Metas, Destino, Plan, Costo):-
	buscarAestrella([nodo(EInicial, [], 0, 0)], [], Metas, Destino, PlanInvertido, Costo),
	reverse(PlanInvertido, Plan).

%%
%%Implementacion de Esquema de Busqueda
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
%%A* pathfinding
%%buscarAestrella(+Frontera, +Visitados, +Metas, -Destino, -Plan, -Costo)
%%Frontera es una lista de estructuras NODO[Estado, Plan, CostoG, CostoH]
buscarAestrella(Frontera, _, Metas, PosDestino, P, G):-
	seleccionar(nodo([PosDestino, Dir], P, G, _), Frontera, _),
	member(PosDestino, Metas),
	%%nl,
	!.
	
buscarAestrella(Frontera, Visitados, Metas, Destino, Plan, Costo):-
	%%nl, write('Entrando en caso recursivo con F = '), writeln(Frontera),
	seleccionar(nodo(Estado, P, G, H), Frontera, FronteraSinNodo),
	%%write('Seleccione a '), write(Estado), write(' de la frontera. FsinN = '), writeln(FronteraSinNodo),
	generarVecinos(nodo(Estado, P, G, H), Metas, Frontera, Visitados, Vecinos),
	%%write('Genere los vecinos de '), write(Estado), write('. Vecinos = '), writeln(Vecinos),
	agregarAestrella(FronteraSinNodo, Vecinos, NuevaFrontera),
	%%write('Agregue los nodos a la frontera. NuevaF = '), writeln(NuevaFrontera),
	buscarAestrella(NuevaFrontera, [Estado|Visitados], Metas, Destino, Plan, Costo).

%%Funcion de costo de camino de A*
f(G, H, F):- F is (G + H). 

%%Funcion Heuristica de costo de camino
%%Recibe una Posicion (dentro de un Estado) y las Metas, 
%%y calcula la Distancia Manhattan a cada Destino del 
%%conjunto de Metas, el costo elegido es el minimo
%%costoH(+Estado, +Metas, -H)
/*
costoH( [[X1, Y1], _], [X2, Y2], H ):- 
	findall(Costo, (, Costo is (abs(X2-X1)+abs(Y2-Y1)) ), TodosLosCostos)
	H is (abs(X2-X1)+abs(Y2-Y1)).
*/
costoH( [[X1, Y1], _], Metas, H ):- 
	findall(
		Costo, 
		( 
			member([X2, Y2], Metas), 
			Costo is (abs(X2-X1)+abs(Y2-Y1)) 
		), 
		TodosLosCostos
	),
	min_member(H, TodosLosCostos).
	
%%Selecciona la cabeza de la frontera y devuelve la cola de la frontera
seleccionar(nodo(Estado, P, G, H), [nodo(Estado, P, G, H)|FronteraSinNodo], FronteraSinNodo).
	
%%Genera una lista de NODOS[EstadoSuc, PlanSuc, GSuc, HSuc] que se obtienen
%%como resultado de ejecutar una Accion (move_fwd o turn(Dir)) valida,
%%a partir de un NODO[Estado, Plan, G, H]. Lo que es equivalente a los vecinos.
%%No esta del todo eficiente, falta comparar el costo de los caminos al volver a visitar un nodo.
%%Mejorar control de ciclos, evitando generar nodos que esten en la Frontera o hayan sido Visitados.
generarVecinos(nodo(Estado, P, G, _), Metas, Frontera, Visitados, Vecinos):-
	findall(
		nodo([PosSuc, DirSuc], [Action|P], GSuc, HSuc), 
		(
			get_action(Estado, [PosSuc, DirSuc], Action, ActionCost),
			costoH([PosSuc, DirSuc], Metas, HSuc),
			GSuc is (G + ActionCost),
			\+member([PosSuc, _], Visitados)
			%%\+member(nodo([PosSuc, _], _, _, HF), Frontera)
		), 
		Vecinos
	).

%%Agrega los Vecinos a la frontera en orden creciente de costos (A*)
%%agregarAestrella(+Frontera, +Vecinos, -NuevaFrontera)
agregarAestrella(FronteraSinNodo, NuevosNodos, NuevaFronteraOrdenada):-
	append_ordenado(FronteraSinNodo, NuevosNodos, NuevaFronteraOrdenada).
	%%append(FronteraSinNodo, NuevosNodos, NuevaFrontera),
	%%ordenar(NuevaFrontera, NuevaFronteraOrdenada).

%get_action(+S1, -S2, -Action, -Cost)
%Dado S1 indica que accion lleva a S2 y con que Cost.

%Siempre se puede girar en cualquier dir.	
get_action([Pos, _], [Pos, n], turn(n), 1).
get_action([Pos, _], [Pos, s], turn(s), 1).
get_action([Pos, _], [Pos, e], turn(e), 1).
get_action([Pos, _], [Pos, w], turn(w), 1).

get_action([Pos, Dir], [PosInFront, Dir], move_fwd, 1) :- 
	ady_at_cardinal(Pos, Dir, PosInFront),						
	land(PosInFront, plain).

get_action([Pos, Dir], [PosInFront, Dir], move_fwd, 2) :- 
	ady_at_cardinal(Pos, Dir, PosInFront),						
	land(PosInFront, mountain).		
	
%%
%%Meotodos de ordenamiento por costo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%Inserta de manera ordenada los elemententos de L2 en L1
%%append_ordenado(+L1, +L2, -L).
append_ordenado(L, [], L).
append_ordenado(L1, [A|R], LOrdenada):-
	append_ordenado(L1, R, L2),
	insertarOrdenado(A, L2, LOrdenada).
	
ordenar([],[]).
ordenar([X|T1],L):- 
	ordenar(T1,T2), 
	insertarOrdenado(X,T2,L).
	
insertarOrdenado(X,[],[X]).
insertarOrdenado(nodo(N, PN, GN, HN), [nodo(M, PM, GM, HM) | R], [nodo(N, PN, GN, HN), nodo(M, PM, GM, HM) | R]):- 
	f(GN, HN, FN), 
	f(GM, HM, FM), 
	FN < FM, !.
insertarOrdenado(X,[Y|T1],[Y|T2]):- 
	insertarOrdenado(X,T1,T2).