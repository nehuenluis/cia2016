%% Player-Agent explorer

:- dynamic turn/1, has/2, at/2, entity_descr/2, land/2, last_seen/2, plan/4, prev_stam/1.

%%Incluye ag_primitives y extras_for_agents
:- consult(ag_primitives), consult(extras_for_agents), consult(explorer_update), consult(explorer_decide).

%%Incluye game_settings para tener informacion acerca de los parametros del juego
%:- consult(../game_settings).

%%Esto no se para que se usa
last_perc(none).

run:-
      get_percept(Perc),

      % AGENT CODE
      % (internal state update and action choice)

      update_state(Perc),

      display_ag, nl,

      decide_action(Action),

      do_action(Action),

      run.
	  

:- dynamic ag_name/1.

%%Punto de Entrada al agente.
%%El primer assert que se produce es el nombre.
start_ag:- AgID = [agent, explorer],
           register_me(AgID, Status),
           !,
           write('REGISTRATION STATUS: '),
           write(Status), nl, nl,
           Status = connected,
           assert(ag_name(explorer)),
           run.

s:- start_ag.

%%Punto de Entrada al agente como instancia.
%%El primer assert que se produce es el nombre.
start_ag_instance(InstanceID):-
                    AgClassName = explorer,
                    AgInstanceName =.. [AgClassName, InstanceID],
                    register_me([agent, AgInstanceName], Status),
                    !,
                    write('REGISTRATION STATUS: '),
                    write(Status), nl, nl,
                    Status = connected,
                    assert(ag_name(AgInstanceName)),
                    run.

si(InstanceID):- start_ag_instance(InstanceID).















