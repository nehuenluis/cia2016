:- consult(explorer_plan).

%%%%%%%%%%%%%%%%%%%%%%
%%
%%	TOMA DE DESICIONES
%%	******************
%%	

%%En hostel me quedo hasta estar cerca de la max stamina.	
decide_action(Action):-
	in_hostel,
	property([agent, me], stamina, AgStamina),
	property([agent, me], max_stamina, AgMaxStamina),
	AgStamina < 0.9 * AgMaxStamina,
	write('Me quedo en el hostel hasta curarme un poco...'), nl,
	Action = null_action,
	write('Action: '), writeln(Action).
	
%%En caso de emergencia, stamina baja. Ir a una posada!
decide_action(Action):-	
	findall(Pos, at([hostel, _], Pos), Hostels),
	at([agent, me], MyPos),
	property([agent, me], dir, MyDir),
	costoH([MyPos, MyDir], Hostels, CostoMasCercano),
	property([agent, me], stamina, AgStamina),
	property([agent, me], max_stamina, AgMaxStamina),
	CostoMasCercano + 0.1 * AgMaxStamina >= AgStamina,
	writeln('Necesito stamina! '),
	\+plan(emergency, _, _, _), 
	retract(plan(_, _, _, _)),%Si se ejecutaba otro plan que no era de emergencia, se quita.
	buscar_plan_desplazamiento([MyPos, MyDir], Hostels, Destino, [Pri|Resto], _),
	write('Espero llegar al hostel en '), write(Destino), nl,
	write('Plan: '), writeln([Pri|Resto]),
	Action = Pri,
	write('Action: '), writeln(Action),
	assert(plan(emergency, Resto, 'Llegar a un hostel', [true])).	

%%En caso de tener un plan, se ejecuta.
decide_action(Action):-		
	(retract(plan(Tipo, [], Obj, Motivos)); plan(Tipo, [Pri|Resto], Obj, Motivos)),		
	retract(plan(Tipo, [Pri|Resto], Obj, Motivos)),
	\+have_died, %Si he muerto, recalculo el plan. 
	forall(member(Cond, Motivos), (call(Cond) ; write('Aborto porque ya no se cumple: '), write(Cond), nl, false)),
	assert(plan(Tipo, Resto, Obj, Motivos)),
	write('OBJETIVO: '), writeln(Obj), write('PLAN: '), write([Pri|Resto]), nl,	
	Action = Pri,
	write('Action: '), writeln(Action).			

%%Se fija que tesoros vio en la memoria. Busca un camino hacia el tesoro mas cercano y activa un plan para ir a buscarlo. 	
decide_action(Action):-
	findall(Pos, (at([treasure, _], Pos), land(Pos, Land), (Land = plain; Land = mountain)), Tesoros), Tesoros \= [],
	write('Vi tesoros en '), write(Tesoros), write(' y los quisiera!'), nl,
	at([agent, me], MyPos),
	property([agent, me], dir, Dir),
	buscar_plan_desplazamiento([MyPos, Dir], Tesoros, Destino, PlanCamino, Costo),
	at([treasure, TrName], Destino),
	append(PlanCamino, [pickup([treasure, TrName])], [Pri|Resto]),
	write('Voy a buscar el mas cercano '), write(' en '), write(Destino), nl,
	write('Plan: '), write([Pri|Resto]), write(' Costo: '), write(Costo), nl,
	Action = Pri,
	write('Action: '), writeln(Action),	
	assert(plan(movement, Resto, 'Recojer un tesoro', [at([treasure, TrName], Destino)])).

%%Se busca una tumba con tesoro que este abierta.
decide_action(Action):-	
	entity_descr([grave, _], [[open, true]]),%Hay alguna?
	findall(Pos, (entity_descr([grave, GrName], [[open, true]]), at([grave, GrName], Pos), has([grave, GrName], [treasure, _])), Tumbas),
	Tumbas \= [],
	at([agent, me], MyPos),
	property([agent, me], dir, Dir),
	buscar_plan_desplazamiento([MyPos, Dir], Tumbas, Destino, PlanCamino, Costo),
	at([grave, GrName], Destino), has([grave, GrName], [treasure, TrName]), %Obtengo la tumba a la que voy y el tesoro que tiene.
	append(PlanCamino, [take_from([treasure, TrName], [grave, GrName])], [Pri|Resto]),	
	write('Voy a la tumba abierta mas cercana con tesoro '), write(' en '), write(Destino), nl,
	write('Plan: '), write([Pri|Resto]), write(' Costo: '), write(Costo), nl,
	Action = Pri,
	write('Action: '), writeln(Action),	
	assert(plan(movement, Resto, 'Retirar tesoro de tumba abierta', [has([grave, GrName], [treasure, TrName]), entity_descr([grave, _], [[open, true]])])).	
	
%%Si tengo una posion de apertura, busco una tumba cerrada pero con tesoro.
decide_action(Action):-	
	has([agent, me], [opening_potion, _]),
	findall(Pos, (at([grave, GrName], Pos), has([grave, GrName], [treasure, _])), Tumbas),
	Tumbas \= [],
	at([agent, me], MyPos),
	property([agent, me], dir, Dir),
	buscar_plan_desplazamiento([MyPos, Dir], Tumbas, Destino, PlanCamino, CostoCamino),
	at([grave, GrName], Destino), has([grave, GrName], [treasure, TrName]), %Obtengo la tumba a la que voy y el tesoro que tiene.
	append(PlanCamino, [cast_spell(open([grave, GrName])), take_from([treasure, TrName], [grave, GrName])], [Pri|Resto]),
	Costo is CostoCamino + 1,%el hechizo. 
	write('Voy a la tumba mas cercana con tesoro '), write(' en '), write(Destino), nl,
	write('Plan: '), write([Pri|Resto]), write(' Costo: '), write(Costo), nl,
	Action = Pri,
	write('Action: '), writeln(Action),	
	assert(plan(movement, Resto, 'Abrir una tumba', [has([grave, GrName], [treasure, TrName])])).

%%Si no tengo una posion de apertura, voy a buscar una.	
decide_action(Action):-	 
	%entity_descr([grave, _], [[open, false]]),
	at([opening_potion, _], _), %%Esto hace que me imprima dos veces por backtracking
	findall(Pos, (at([opening_potion, _], Pos), land(Pos, Land), (Land = plain; Land = mountain)), Posiones),
	Posiones \= [],
	write('Vi las posiones de apertura. '), write(Posiones), write(' y necesito alguna!'), nl,
	at([agent, me], MyPos),
	property([agent, me], dir, Dir),
	buscar_plan_desplazamiento([MyPos, Dir], Posiones, Destino, PlanCamino, Costo),
	at([opening_potion, PotName], Destino),
	append(PlanCamino, [pickup([opening_potion, PotName])], [Pri|Resto]),
	write('Voy a buscar la mas cercana '), write(' en '), write(Destino), nl,
	write('Plan: '), write([Pri|Resto]), write(' Costo: '), write(Costo), nl,
	Action = Pri,
	write('Action: '), writeln(Action),	
	assert(plan(movement, Resto, 'Recojer una posion de apertura', [at([opening_potion, PotName], Destino)])).
	

%%%%%%%%%%%%%%%%%%%%%%
%%
%%	Exploracion (en el caso de que todo lo demas falle)
%%	******************
%%	
	
%%Encontrar entre las posiciones que conozco todas aquellas que tengan
%%una posicion adyacente no explorada (no esta en la memoria)
%%Y buscar un plan para movernos al mas cercano a explorar
decide_action(Action):-	
	findall(
		Pos, 
		(
			land(Pos, Land),
			(
				Land = plain
					;
				Land = mountain
			),
			ady_at_cardinal(Pos, _, PosNoExplorada),
			\+land(PosNoExplorada, _)
		), 
		Frontera
	),
	%%Si no explore todo el mapa
	Frontera \= [],
	at([agent, me], MyPos),
	property([agent, me], dir, MyDir),
	buscar_plan_desplazamiento([MyPos, MyDir], Frontera, Destino, [Pri|Resto], Costo),
	write('Voy a explorar en '), write(Destino), nl,
	write('Plan: '), writeln([Pri|Resto]),
	Action = Pri,
	write('Action: '), writeln(Action),
	assert(plan(movement, Resto, 'Explorar', [true])).
	
decide_action(Action):-	
	writeln('Ya explore todo el mapa y no tengo mas metas'),
	writeln('Mi vida no tiene sentido...'),
	Action = null_action,
	write('Action: '), writeln(Action).

%%%%%%%%%%%%%%%%%%%%%%
%%
%%	Auxiliares
%%	******************
%%

%%Si tengo mas stamina que en el turno anterior sin estar en un hostel, es porque he muerto. 
have_died :- 
	prev_stam(PS),%La stamina en el turno anterior. 
	retract(prev_stam(PS)),
	property([agent, me], stamina, AgStamina),
	assert(prev_stam(AgStamina)),
	PS \= 0,%El valor se inicio en cero, luego no es nunca 0. 		
	\+in_hostel,
	AgStamina > PS,
	writeln('He resucitado! Que hago ahora...?'). 

in_hostel :- 
	at([agent, me], MyPos),
	at([hostel, _], MyPos).