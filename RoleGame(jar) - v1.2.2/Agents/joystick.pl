%% Player-Agent joystick

:- consult(ag_primitives), consult(extras_for_agents).

run:-
      get_percept(Perc),

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % AGENT CODE
      % (internal state update and action choice)

      writeln(Perc), nl,

%      display_ag(AgName, Perc), nl,

      write('ACCION?: '), read(Action),

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      do_action(Action),

      run.


:- dynamic ag_name/1.


start_ag:- AgName = jk,
	   AgID = [agent, AgName],
           register_me(AgID, Status),
           !,
           write('REGISTRATION STATUS: '),
           write(Status), nl, nl,
           Status = connected,
           assert(ag_name(AgName)),
           run.

s:- start_ag.


start_ag_instance(InstanceID):-
                    AgClassName = jk,
                    AgInstanceName =.. [AgClassName, InstanceID],
		    AgInstanceID = [agent, AgInstanceName],
                    register_me(AgInstanceID, Status),
                    !,
                    write('REGISTRATION STATUS: '),
                    write(Status), nl, nl,
                    Status = connected,
                    assert(ag_name(AgInstanceName)),
                    run.

si(InstanceID):- start_ag_instance(InstanceID).



