

%%%%%%%%%%%%%%%%%%%%%%
%%
%%	ACTUALIZACION DE BD	
%%	******************
%%	  

%Para que el primer retract(turn(_)) no falle. 
turn(0).
prev_stam(0).
	  
update_state(Perc):-
	%Guardo el turno, solo por como lo piden en extras_for_agents.
	retract(turn(_)), member(turn(T), Perc), assert(turn(T)),	
	
	writeln('-------------------------------------------------------------'),
		
	%Se recuerda el mapa a medida que se explora.
	findall(land(Pos, Land), 
	(
		member(land(Pos, Land), Perc), 
		\+land(Pos, Land)
	), NewLand),
	forall(member(land(Pos, Land), NewLand), assert(land(Pos, Land))),
	
	%Borro aquellos has(Owner, Object) cuya Owner ha cambiado...
	findall(has(OldOwner, Object), 
	(
		member(has(NewOwner, Object), Perc), 
		has(OldOwner, Object), 
		OldOwner \= NewOwner
	),OldHases), 
	%write('OldHases: '), write(OldHases), nl,
	forall(member(has(Owner, Object), OldHases), retract(has(Owner, Object))),
	
	%Borro aquellos has(Owner, Object) que "deberian" estar en la percepcion pero no estan, ej: vi que alguien ya no tiene un Objecto.
	findall(has(Owner, Object), 
	(
		has(Owner, Object), 
		member(at(Owner, _), Perc), 
		\+member(has(Owner, Object), Perc)
	), InvalidHases), 
	%write('InvalidHases: '), write(InvalidHases), nl,
	forall(member(has(Owner, Object), InvalidHases), retract(has(Owner, Object))),
	
	%Borro aquellos has(Owner, Object) cuando me encuentro con ese Object tirado.
	findall(has(Owner, Object), 
	(
		has(Owner, Object), 
		member(at(Object, _), Perc)
	), DroppedObjects), 
	%write('DroppedObjects: '), write(DroppedObjects), nl,
	forall(member(has(Owner, Object), DroppedObjects), retract(has(Owner, Object))),
	
	%Agrego de la percepcion los has(Owner, Object) que cambiaron, y los que aun no tengo en la BD.  
	findall(has(Owner, Object), 
	(
		member(has(Owner, Object), Perc), 
		\+has(Owner, Object)
	), NewHases), 
	%write('NewHases: '), write(NewHases), nl,
	forall(member(has(Owner, Object), NewHases), assert(has(Owner, Object))),
	
	%Borro aquellos at's cuya Pos ha cambiado...Ej: un agente que se movio.
	findall(at(Entity, OldPos), 
	(
		member(at(Entity, NewPos), Perc), 
		at(Entity, OldPos), 
		OldPos \= NewPos
	), OldAts), 
	%write('OldAts: '), write(OldAts), nl,
	forall(member(at(Entity, Pos), OldAts), retract(at(Entity, Pos))),
	
	%Borro aquellos at's que "deberian" estar en la percepcion pero no estan, ej: un tesoro que se llevaron, un agente que ya no se donde esta.
	findall(at(Entity, Pos), 
	(
		at(Entity, Pos), 
		member(land(Pos, _), Perc), 
		\+member(at(Entity, Pos), Perc)
	), InvalidAts), 
	%write('InvalidAts: '), write(InvalidAts), nl,
	forall(member(at(Entity, Pos), InvalidAts), retract(at(Entity, Pos))),
	
	%Y si es una posion y ni se quien la tiene, borro su Descr. Es inutil.
	findall(entity_descr(Entity, Descr), 
	(
		member(at(Entity, _), InvalidAts), 
		Entity = [EntityType, _], 
		is_a(EntityType, potion), 
		\+has(_, Entity)
	), UselessDescrs), 
	%write('UselessDescrs: '), write(UselessDescrs), nl,
	forall(member(entity_descr(Entity, Descr), UselessDescrs), (retract(entity_descr(Entity, Descr)), retract(last_seen(Entity, _)))),
	
	%Si veo que alguien tiene un Objecto que yo habia visto tirado, debo borrar esa creencia. 
	findall(at(Entity, _), 
	(
		at(Entity, _), 
		member(has(_, Entity), Perc)
	), TakenObjs), 
	%write('TakenObjs: '), write(TakenObjs), nl,
	forall(member(at(Entity, _), TakenObjs), (retract(at(Entity, _)), retract(last_seen(Entity, _)))), %Entity1 tiene Entity2, alcanza con un solo last_seen.
	
	%Agrego de la percepcion los at's que cambiaron, y los que aun no tengo en la BD.  
	findall(at(Entity, Pos), 
	(
		member(at(Entity, Pos), Perc), 
		\+at(Entity, Pos)
	), NewAts), 
	%write('NewAts: '), write(NewAts), nl,
	forall(member(at(Entity, Pos), NewAts), assert(at(Entity, Pos))),
	
	%Borro aquellos entity_descr's cuya Descr ha cambiado...
	findall(entity_descr(Entity, OldDescr), 
	(
		member(entity_descr(Entity, NewDescr), Perc), 
		entity_descr(Entity, OldDescr), 
		OldDescr \= NewDescr
	), OldDescrs),
	forall(member(entity_descr(Entity, Descr), OldDescrs), retract(entity_descr(Entity, Descr))),
	
	%Agrego de la percepcion los entity_descr's que cambiaron, y los que aun no tengo en la BD.  
	findall(entity_descr(Entity, Descr), 
	(
		member(entity_descr(Entity, Descr), Perc), 
		\+entity_descr(Entity, Descr)
	), NewDescrs),
	forall(member(entity_descr(Entity, Descr), NewDescrs), assert(entity_descr(Entity, Descr))),
	
	%Quiero saber cuando fue la ultima vez que vi una entidad.
	%Si veo a alguien nuevamente debo actualizar esa creencia, borrando la anterior. 
	findall(last_seen(Entity, turn(PrevTurn)), 
	(
		turn(ActualTurn), 
		last_seen(Entity, turn(PrevTurn)), 
		ActualTurn \= PrevTurn, 
		member(at(Entity, _), Perc)
	), Seens),
	%write('Seens (a borrar): '), write(Seens), nl,
	forall(member(last_seen(Entity, turn(PrevTurn)), Seens), retract(last_seen(Entity, turn(PrevTurn)))),
	%Ahora tengo que guardarlos con el turno actual, y tambien guardo las que no habia visto antes que estan en Perc.
	%write('NewSeens: '),
	forall(member(at(Entity, _), Perc), (turn(ActualTurn), assert(last_seen(Entity, turn(ActualTurn))))). 
	%write(last_seen(Entity, turn(ActualTurn))))), nl. 